package at.jku.tk.mms.huffman;

import java.util.ArrayList;
import java.util.Map;

import at.jku.tk.mms.huffman.impl.BitCode;
import at.jku.tk.mms.huffman.impl.BitCodeArray;
import at.jku.tk.mms.huffman.impl.BitStream;
import at.jku.tk.mms.huffman.impl.FreqencyTable;
import at.jku.tk.mms.huffman.impl.HuffmanTree;
import at.jku.tk.mms.huffman.impl.TreeNode;

/**
 * Implementation of the Huffman Encoding
 * 
 * @author matthias
 */
public class HuffmanCompression {
	
	private FreqencyTable freqTable;
	
	private HuffmanTree huffmanTree;

	/**
	 * Creates a new Huffman Encoder initialized with plain byte arr for frequency table creation
	 * 
	 * @param ipt
	 */
	public HuffmanCompression(byte[] ipt) {
		this.freqTable = new FreqencyTable(ipt);
		this.huffmanTree = new HuffmanTree(this.freqTable);
	}
	
	/** Getter for Frequency Table */
	public FreqencyTable getFreqencyTable() {
		return this.freqTable;
	}
	
	/** Getter for Huffman Tree */
	public HuffmanTree getHuffmanTree() {
		return this.huffmanTree;
	}
	
	/** Encode with huffman lookup table */
	public byte[] compress(byte[] plain) {
		Map<Byte, BitCode> lookupTable = huffmanTree.getLookupTable();
		BitCodeArray arr = new BitCodeArray();		

		for(byte b : plain) {
			arr.append(lookupTable.get(b));
		}	
		
		return arr.toByteArray();
	}
	
	/** Decode with huffman lookup table */
	public byte[] decompress(byte[] compressed) {
		ArrayList<Byte> buffer = new ArrayList<Byte>();
		BitStream stream = new BitStream(compressed);
		TreeNode current = huffmanTree.getRootNode();

		while (stream.canRead()) {
			current = stream.readBit() == 0 ? current.getLeft() : current.getRight();

			if (current.isLeaf()) {
				buffer.add(current.getValue());
				current = huffmanTree.getRootNode();
			}
		}

		byte[] result = new byte[buffer.size()];
		
		for (int i = 0; i < buffer.size(); i++) {
			result[i] = buffer.get(i);
		}

		return result;
	}
}
