package at.jku.tk.mms.huffman.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains the huffman tree
 * 
 * @author matthias
 */
public class HuffmanTree {
	
	private TreeNode root;
	
	private Map<Byte, BitCode> lookup;

	/** Initialize HuffmanTree with FrequencyTable */
	public HuffmanTree(FreqencyTable ftable) {
		if(ftable == null) {
			throw new IllegalArgumentException("Cannot initialize Huffman Tree with null frequency table");
		}
		List<TreeNode> t = ftable.getTable();
		if(t.size() == 0) {
			throw new IllegalArgumentException("Cannot initialzie Huffman Tree with empty frequency table");
		}
		initializeTree(t);
		initializeLookup();
	}
	
	/** Initialize the huffman tree */
	private void initializeTree(List<TreeNode> t) {
		int minFreq = 0, minFreq2 = 0;
		TreeNode treeNode1 = null, treeNode2 = null;

		while (t.size() > 1) {
			minFreq = 0;
			minFreq2 = 0;
			for (TreeNode treeNode : t) {			
				if (treeNode.getFreq() < minFreq || minFreq == 0) {
					minFreq = treeNode.getFreq();
					treeNode1 = treeNode;
				}
				else if(treeNode.getFreq() < minFreq2 || minFreq2 == 0) {
					minFreq2 = treeNode.getFreq();
					treeNode2 = treeNode;
				}					
			}
			TreeNode newTreeNode = new TreeNode((byte) -1, treeNode1.getFreq() + treeNode2.getFreq(), treeNode1, treeNode2);
			t.remove(treeNode2);
			t.remove(treeNode1);
			t.add(newTreeNode);
		}
		root = t.get(0);
	}

	/** Iterate over tree and init lookup */
	private void initializeLookup() {
		lookup = new HashMap<Byte, BitCode>();
		root.initLookup((byte) 0, 0, lookup);
	}
	
	public TreeNode getRootNode() {
		return root;
	}
	
	public Map<Byte, BitCode> getLookupTable() {
		return lookup;
	}
	
	/** Debug print tree */
	public void printTree() {
		root.printSubTree(0);
	}
	
	/** debug print lookup table */
	public void printLookupTable() {
		for(Byte cur : lookup.keySet()) {
			BitCode code = lookup.get(cur);
			char curChar = (char) (int) cur;
			System.out.println("value: " + cur + " (char) " + curChar + " encoded as " + code);
		}
	}
	
}
