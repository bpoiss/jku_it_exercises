package at.jku.tk.mms.huffman.test;

import static org.junit.Assert.*;

import org.junit.Test;

import at.jku.tk.mms.huffman.HuffmanCompression;
import at.jku.tk.mms.huffman.impl.BitStream;

/**
 * Test the {@link HuffmanCompression} classes
 *  
 * @author matthias
 */
public class HuffmanCompressionTest {
	
	public static final String TEST_INPUT = "Two goldfish are in a tank. One says to the other, \"Do you know how to drive this thing?\"";
	public static final String TEST_INPUT_SHORT = "MMS";
	public static final String TEST_ABRACADABRA = "abracadabra";

	@Test
	public void testFrequencyTable() {
		HuffmanCompression compressor = new HuffmanCompression(TEST_INPUT.getBytes());
		assertEquals(9,  compressor.getFreqencyTable().getFrequency((byte) 'o'));
		assertEquals(18, compressor.getFreqencyTable().getFrequency((byte) ' '));
		assertEquals(2,  compressor.getFreqencyTable().getFrequency((byte) '"'));
		assertEquals(1,  compressor.getFreqencyTable().getFrequency((byte) '?'));
		assertEquals(1,  compressor.getFreqencyTable().getFrequency((byte) 'T'));
	}
	
	@Test
	public void testFrequencyTableShort() {
		HuffmanCompression compressor = new HuffmanCompression(TEST_INPUT_SHORT.getBytes());
		assertEquals(2, compressor.getFreqencyTable().getFrequency((byte) 'M'));
		assertEquals(1, compressor.getFreqencyTable().getFrequency((byte) 'S'));
	}
	
	@Test
	public void testBitStreamWriting() {
		BitStream s = new BitStream();
		s.setDebug(true);
		s.writeBit((byte)0);// byte 1 / val 85
		s.writeBit((byte)1);
		s.writeBit((byte)0);
		s.writeBit((byte)1);
		s.writeBit((byte)0);
		s.writeBit((byte)1);
		s.writeBit((byte)0);
		s.writeBit((byte)1);
		s.writeBit((byte)0);// byte 2 / val 85
		s.writeBit((byte)1);
		s.writeBit((byte)0);
		s.writeBit((byte)1);
		s.writeBit((byte)0);
		s.writeBit((byte)1);
		s.writeBit((byte)0);
		s.writeBit((byte)1);
		s.writeBit((byte)0);// byte 3 / val 64
		s.writeBit((byte)1);// fill with 0
		s.flush();
		
		byte[] arr = s.toByteArray();
		assertEquals(3, arr.length);
		assertEquals(85, arr[0]);
		assertEquals(85, arr[1]);
		assertEquals(64, arr[2]);
	}
	
	@Test
	public void testBitStreamReading() {
		byte[]arr = new byte [] { 85, 85, 64 };
		BitStream s = new BitStream(arr);
		s.setDebug(true);
		String cur = "";
		int i=0;
		while(s.canRead()) {
			if(s.readBit() > 0) {
				cur += "1";
			}else{
				cur += "0";
			}
			i++;
		}
		assertEquals(24, i);
		assertEquals("010101010101010101000000", cur);
	}
	
	@Test
	public void testBitStreamInOut() {
		BitStream in = new BitStream(TEST_INPUT.getBytes());
		in.setDebug(true);
		BitStream out = new BitStream();
		out.setDebug(true);
		int i=0;
		while(in.canRead()) {
			byte bit = in.readBit();
			out.writeBit(bit);
			i++;
		}
		out.flush();
		assertEquals(TEST_INPUT.length() * 8, i);
		String str = new String(out.toByteArray());
		assertEquals(TEST_INPUT, str);
	}
	
	@Test
	public void testCompressionInOutShort() {
		HuffmanCompression compressor = new HuffmanCompression(TEST_INPUT_SHORT.getBytes());
		byte[] compressed = compressor.compress(TEST_INPUT_SHORT.getBytes());
		byte[] uncompressed = compressor.decompress(compressed);
		String result = new String(uncompressed);
		assertEquals(TEST_INPUT_SHORT, result.substring(0, TEST_INPUT_SHORT.length()));
		System.out.println("Compression result: " + TEST_INPUT_SHORT.getBytes().length + " vs " + compressed.length);
	}
	
	@Test
	public void testCompressionInOutLong() {
		HuffmanCompression compressor = new HuffmanCompression(TEST_INPUT.getBytes());
		byte[] compressed = compressor.compress(TEST_INPUT.getBytes());
		byte[] uncompressed = compressor.decompress(compressed);
		String result = new String(uncompressed);
		assertEquals(TEST_INPUT, result.substring(0, TEST_INPUT.getBytes().length));
		System.out.println("Compression result: " + TEST_INPUT.getBytes().length + " vs " + compressed.length);
	}
	
	@Test
	public void testCompressionInOutAbracadabra() {
		HuffmanCompression compressor = new HuffmanCompression(TEST_ABRACADABRA.getBytes());
		byte[] compressed = compressor.compress(TEST_ABRACADABRA.getBytes());
		byte[] uncompressed = compressor.decompress(compressed);
		String result = new String(uncompressed);
		assertEquals(TEST_ABRACADABRA, result.substring(0, TEST_ABRACADABRA.getBytes().length));
		System.out.println("Compression result: " + TEST_ABRACADABRA.getBytes().length + " vs " + compressed.length);
	}
	
}
