package at.jku.tk.mms.img.filters;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Properties;

import at.jku.tk.mms.img.FilterInterface;
import at.jku.tk.mms.img.pixels.Pixel;

/** Apply sub sampling of color values only */
public class ChromaSubsamling implements FilterInterface {

	@Override
	public Image runFilter(BufferedImage img, Properties settings) {
		int ratecb = Integer.parseInt(settings.getProperty("horizontal"));
		int ratecr = Integer.parseInt(settings.getProperty("vertical"));
		
		BufferedImage subsampled = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);

		int rgb = 0, rgbNew;
		boolean flag = false;
		int y;
		Pixel pixel, pixelNew;
		
		for(int i = 0; i + ratecb <= img.getWidth(); i++) {
			for(int j = 0; j < img.getHeight(); j++) {
				for(int tmp = 0; tmp <= ratecr && j < img.getHeight(); tmp++) {
					pixel = new Pixel(img.getRGB(i, j - tmp));
					pixelNew = new Pixel(img.getRGB(i, j));
					pixelNew.setY(pixel.getY());
					subsampled.setRGB(i, j, pixelNew.getRawRGBA());
					j++;
					flag = true;
				}
				if (flag) {
					j--;
					flag = false;
				}
			}
			
			i++;
			
			for(int tmp = 0; tmp < ratecb && i > 0 && i < img.getWidth(); tmp++) {
				for(int j = 0; j < img.getHeight(); j++) {
					rgb = img.getRGB(i - tmp, j);
					pixel = new Pixel(rgb);
					pixelNew = new Pixel(img.getRGB(i, j));
					pixelNew.setY(pixel.getY());
					subsampled.setRGB(i, j, pixelNew.getRawRGBA());	
				}				
				i++;
			}
			i--;
		}
		
		return subsampled;
	}

	@Override
	public String[] mandatoryProperties() {
		return new String [] { "horizontal:s:1-8:2", "vertical:n:1-8:2" };
	}
	
	@Override
	public String toString() {
		return "subsampling - chroma";
	}

}
