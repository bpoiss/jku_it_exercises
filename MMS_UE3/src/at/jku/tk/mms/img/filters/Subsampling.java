package at.jku.tk.mms.img.filters;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Properties;

import at.jku.tk.mms.img.FilterInterface;

/** Perform sub sampling on the image */
public class Subsampling implements FilterInterface {

	@Override
	public Image runFilter(BufferedImage img, Properties settings) {
		int rate = Integer.parseInt(settings.getProperty("rate"));		
		BufferedImage bufferedImage = new BufferedImage(img.getWidth() / rate, img.getHeight() / rate, BufferedImage.TYPE_INT_ARGB);
		int k = 0, l = 0;
		for(int i = 0; i + rate <= img.getWidth(); i += rate) {
			l = 0;
			for(int j = 0; j + rate <= img.getHeight(); j += rate) {
				bufferedImage.setRGB(k, l, img.getRGB(i, j));
				l++;
			}
			k++;
		}
		
		return bufferedImage;
	}

	@Override
	public String[] mandatoryProperties() {
		return new String [] { "rate:n:1-8:2" };
	}
	
	@Override
	public String toString() {
		return "subsampling";
	}

}
