package at.jku.tk.mms.img.filters;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import java.util.Properties;

import at.jku.tk.mms.img.FilterInterface;
import at.jku.tk.mms.img.pixels.Pixel;

/** Filter that implements image thresholding */
public class Threshold implements FilterInterface {

	@Override
	public String[] mandatoryProperties() {
		return new String[] { "threshold:n:0-255:128" };
	}

	@Override
	public String toString() {
		return "threshold";
	}

	@Override
	public Image runFilter(BufferedImage img, Properties settings) {
		int threshold = Integer.parseInt(settings.getProperty("threshold"));
		int whitePixel = Pixel.generateRGBAPixel(255, 255, 255, 255);
		int blackPixel = Pixel.generateRGBAPixel(0, 0, 0, 255);
		int average;
		Color color;
		
		BufferedImage bufferedImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
				
		for(int i = 0; i < img.getWidth(); i++) {
			for(int j = 0; j < img.getHeight(); j++) {
				color = new Color(img.getRGB(i, j));
				average = (color.getRed() + color.getGreen() + color.getBlue()) / 3;
				average = average > 255 ? 255 : average < 0 ? 0 : average;
				bufferedImage.setRGB(i, j, average < threshold ? blackPixel : whitePixel);
			}
		}
		
		return bufferedImage;
	}

}
