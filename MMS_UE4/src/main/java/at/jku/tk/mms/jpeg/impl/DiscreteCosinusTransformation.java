package at.jku.tk.mms.jpeg.impl;

import at.jku.tk.mms.jpeg.helper.PrintableBlock;


public class DiscreteCosinusTransformation {
	
	private int[] quantumLuminance;
	private double[] divLuminance;
	
	private int[] quantumChrominance;
	private double[] divChrominance;
	
	private int blockCount = 0;

	public DiscreteCosinusTransformation() {
		this(Constants.JPEG_DEFAULT_QUALITY);
	}

	public DiscreteCosinusTransformation(int quality) {
	    if(quality <= 0) {
	    	quality = 1;
	    }
	    if(quality > 100) {
	    	quality = 100;
	    }
	    if(quality < 50) {
	    	quality = 5000 / quality;
	    }else{
	      quality = 200 - quality * 2;
	    }
		
		quantumLuminance = qualityAdaption(Constants.JPEG_QUANTUM_LUMINANCE, quality);
		quantumChrominance = qualityAdaption(Constants.JPEG_QUANTUM_CHROMINANCE, quality);
		divLuminance = setupDivisors(quantumLuminance);
		divChrominance = setupDivisors(quantumChrominance);
	}
	
	private int[] qualityAdaption(int[] input, int quality) {
		int[] result = new int[input.length];
		int tmp;
		for(int i=0;i<input.length;i++) {
			tmp = (input[i] * quality + 50) / 100;
			if(tmp <= 0) tmp = 1;
			if(tmp >= 255) tmp = 255;
			result[i] = tmp;
		}
		return result;
	}
	
	private double[] setupDivisors(int[] input) {
		double[] result = new double[Constants.JPEG_BLOCK_SIZE * Constants.JPEG_BLOCK_SIZE];
	    int index = 0;
	    for(int i=0;i<Constants.JPEG_BLOCK_SIZE;i++) {
	    	for(int j=0;j<Constants.JPEG_BLOCK_SIZE;j++) {
	    		result[index] = (1.0/(input[index] * Constants.JPEG_AAN_SCALE_FACTOR[i] * Constants.JPEG_AAN_SCALE_FACTOR[j] * 8.0));
	    		index++;
	    	}
	    }
	    return result;
	}
	
	public double[][] forwardDCT(float[][] block) {
		double[][] result = new double[Constants.JPEG_BLOCK_SIZE][Constants.JPEG_BLOCK_SIZE];
		double[][] dct = new double[Constants.JPEG_BLOCK_SIZE][Constants.JPEG_BLOCK_SIZE];
		
		for(int i=0;i<Constants.JPEG_BLOCK_SIZE;i++) {
			for(int j=0;j<Constants.JPEG_BLOCK_SIZE;j++) {
				// shift values from [0, 255] to [-128, 127]
				result[i][j] = block[i][j] - 128.0;
			}
		}
		
		final int n = 8;
		double tmp = 0;
		
		for(int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				tmp = 0;
				for (int k = 0; k < n; k++) {
					for (int l = 0; l < n; l++) {
						tmp += (double) 2/n * this.C(i) * this.C(j) * result[k][l] * (Math.cos(((2 * k + 1) * i * Math.PI) / (2 * n)) * Math.cos(((2 * l + 1) * j * Math.PI) / (2 * n)));
					}
				}
				dct[i][j] = tmp * 8;
			}
		}

		return dct;
	}
	

	public int[] quantize(double[][] block, double[] divisors) {
		int[] result = new int[Constants.JPEG_BLOCK_SIZE * Constants.JPEG_BLOCK_SIZE];
		
		int k = 0;
		for(int i = 0; i < Constants.JPEG_BLOCK_SIZE; i++) {
			for(int j = 0; j < Constants.JPEG_BLOCK_SIZE; j++) {
				result[k] = (int) Math.round(block[i][j] * divisors[k]);
				k++;
			}
		}
		
	    return result;
	}
	
	public int[] quantizeLuminance(double[][] block) {
		return quantize(block, divLuminance);
	}
	
	public int[] quantizeChrominance(double[][] block) {
		return quantize(block, divChrominance);
	}
	
	public int[] dctQuantLuminance(float[][] block) {
		return quantizeLuminance(forwardDCT(block));
	}
	
	public int[] dctQuantChrominance(float[][] block) {
		return quantizeChrominance(forwardDCT(block));
	}
	
	public int[] getQuantumLuminance() {
		return quantumLuminance;
	}
	
	public int[] getQuantumChrominance() {
		return quantumChrominance;
	}
	
	private double C(int x) {
		return x == 0 ? 1/Math.sqrt(2) : 1;
	}
}
