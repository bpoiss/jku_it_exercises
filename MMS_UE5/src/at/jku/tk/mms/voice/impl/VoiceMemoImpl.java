package at.jku.tk.mms.voice.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.TargetDataLine;

import at.jku.tk.mms.voice.VoiceMemo;
import at.jku.tk.mms.voice.model.Recording;

public class VoiceMemoImpl implements Runnable {

	private boolean playing, recording;

	private AudioFormat audioFormat;

	private float fFrameRate = 44100.0F;

	private Recording lastRecording;

	private Recording nextToPlay;

	private VoiceMemo ui;

	public VoiceMemoImpl(VoiceMemo ui) {
		this.ui = ui;
		playing = false;
		recording = false;
		audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
				fFrameRate, 16, 2, 4, fFrameRate, false);
	}

	public boolean isPlaying() {
		return playing;
	}

	public boolean isRecoding() {
		return recording;
	}

	public void startRecording() {
		recording = true;
		new Thread(this).start();
	}

	public void stopRecording() {
		recording = false;
	}

	public void startPlaying() {
		playing = true;
		new Thread(this).start();
	}

	public void stopPlaying() {
		playing = false;
	}

	public synchronized Recording getLastRecording() {
		try {
			wait();
			return lastRecording;
		} catch (InterruptedException e) {
		}
		return null;
	}

	private synchronized void setLastRecording(Recording r) {
		this.lastRecording = r;
		notify();
	}

	public void setNextToPlay(Recording r) {
		this.nextToPlay = r;
	}

	@Override
	public void run() {
		if (playing) {
			threadPlaying();
		}
		if (recording) {
			threadRecording();
		}

	}

	private void threadPlaying() {
		ByteArrayInputStream stream = new ByteArrayInputStream(
				nextToPlay.getPcmAudio());

		DataLine.Info info = new DataLine.Info(SourceDataLine.class,
				audioFormat);
		SourceDataLine line;
		
		try {
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open();
			line.start();
			int bufferSize = (int) audioFormat.getSampleRate()
					* audioFormat.getFrameSize();
			byte buffer[] = new byte[bufferSize];

			int count;
			while ((count = stream.read(buffer, 0, buffer.length)) != -1) {
				if (count > 0) {
					line.write(buffer, 0, count);
				}
			}
			line.drain();
			line.close();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			playing = false;
		}

		ui.updateUi();
	}

	private void threadRecording() {
		Recording r = new Recording();
		TargetDataLine line;

		DataLine.Info info = new DataLine.Info(TargetDataLine.class,
				audioFormat);

		try {
			line = (TargetDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
			line.start();

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			int bufferSize = (int) audioFormat.getSampleRate()
					* audioFormat.getFrameSize();
			byte buffer[] = new byte[bufferSize];

			while (recording) {
				int count = line.read(buffer, 0, buffer.length);
				if (count > 0) {
					out.write(buffer, 0, count);
				}
			}
			line.stop();
			line.close();
			r.setPcmAudio(out.toByteArray());
			setLastRecording(r);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ui.updateUi();

	}

}
