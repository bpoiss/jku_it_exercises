package at.jku.tk.mms.xuggler;

import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import at.jku.tk.mms.xuggler.helper.Tools;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.ICodec.ID;

/**
 * Class capable of creating stop motion animations
 * 
 */
public class StopMotionApp {
	
	private final File directory, target;
	
	/**
	 * Initialize class
	 * At this point the input params are checked
	 * 
	 * @param directory
	 * @param target
	 */
	public StopMotionApp(File directory, File target) {
		this.directory = directory;
		this.target = target;
	}
	

	private void createAnimation(int delayBetweenImages) throws IOException {
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final Rectangle screenBounds = new Rectangle(toolkit.getScreenSize());
		final IMediaWriter writer = ToolFactory.makeWriter(target.toString());
		long start = System.nanoTime();

		writer.addVideoStream(0, 0, ID.CODEC_ID_H264, screenBounds.width,
				screenBounds.height);

		File[] dirList = directory.listFiles();
		Arrays.sort(dirList);
		
		for (File file : dirList) {
			BufferedImage s = ImageIO.read(file);
			s = Tools.convertToType(s, BufferedImage.TYPE_3BYTE_BGR);
			writer.encodeVideo(0, s, System.nanoTime() - start,
					TimeUnit.NANOSECONDS);
			try {
				Thread.sleep(delayBetweenImages);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		writer.close();
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		if(args.length != 2) {
			throw new IllegalArgumentException("You need to pass a directory of images and a target movie file");
		}
		File directory = new File(args[0]);
		if(directory.exists() && directory.isDirectory()) {
			File target = new File(args[1]);
			StopMotionApp app = new StopMotionApp(directory, target);
			app.createAnimation(250);
		}else{
			throw new IllegalArgumentException("First argument '" + args[0] + "' did not denote an existing directory");
		}
	}

}
