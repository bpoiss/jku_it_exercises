package at.jku.tk.mms.xuggler;

import java.io.File;

import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec.ID;

public class VideoTranscoderApp {

	private File source;

	private File target;

	public VideoTranscoderApp(File source, File target) {
		this.source = source;
		this.target = target;
	}

	public void transcode() {
		IMediaReader reader = ToolFactory.makeReader(source.toString());
		IMediaWriter writer = ToolFactory.makeWriter(target.toString(), reader);
		writer.getContainer().setForcedVideoCodec(ID.CODEC_ID_H264);
		reader.addListener(writer);
		while (reader.readPacket() == null);
	}

	public static void main(String[] args) {
		File source = new File(args[0]);
		if (source.canRead()) {
			File target = new File(args[1]);
			VideoTranscoderApp transcoder = new VideoTranscoderApp(source,
					target);
			transcoder.transcode();
		}
	}

}
