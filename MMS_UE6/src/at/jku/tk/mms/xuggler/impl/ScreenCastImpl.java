package at.jku.tk.mms.xuggler.impl;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

import at.jku.tk.mms.xuggler.helper.Tools;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec.ID;

/**
 * Is capable of recording a Screencast for a defined amount of time
 * 
 * @author matthiassteinbauer
 */
public class ScreenCastImpl {

	private ScreenshotUtility screenshot;
	private final String outputFile;
	private final long seconds;

	/**
	 * Creates a screen cast impl which is capable of recording the screen for a
	 * defined amount of time.
	 * 
	 * File format should be H.264 in an .mp4 file.
	 * 
	 * @param outputFile
	 * @param seconds
	 * @throws AWTException
	 */
	public ScreenCastImpl(String outputFile, long seconds) throws AWTException {
		this.outputFile = outputFile;
		this.seconds = seconds;
		if (!outputFile.toLowerCase().endsWith(".mp4")) {
			throw new IllegalArgumentException(
					"Only .mp4 files can get created by this utility");
		}
		screenshot = new ScreenshotUtility();
	}

	/**
	 * Starts the recording and automatically stops after the recording is done
	 * 
	 * @throws InterruptedException
	 */
	public void startRecording() throws InterruptedException {
		final Toolkit toolkit = Toolkit.getDefaultToolkit();
		final Rectangle screenBounds = new Rectangle(toolkit.getScreenSize());
		final IMediaWriter writer = ToolFactory.makeWriter(outputFile);
		long end = System.nanoTime() + seconds * (1000 * 1000 * 1000);
		long start = System.nanoTime();

		writer.addVideoStream(0, 0, ID.CODEC_ID_H264, screenBounds.width,
				screenBounds.height);

		while (System.nanoTime() < end) {
			BufferedImage s = screenshot.getScreenShot();
			s = Tools.convertToType(s, BufferedImage.TYPE_3BYTE_BGR);
			writer.encodeVideo(0, s, System.nanoTime() - start,
					TimeUnit.NANOSECONDS);
			Thread.sleep(40); // 25 fps
		}
		writer.close();
	}

}
