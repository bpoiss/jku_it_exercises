package project;

public class Item {
	private final String id;
	private final String name;
	private final int duration;
	private ItemList dependsOn;

	/**
	 * @param id the unique id for the item 
	 * @param name
	 * @param duration
	 */
	Item(String id, String name, int duration) {
		dependsOn = new ItemList(); 
		this.id = id;
		this.name = name;
		this.duration = duration < 0 ? 0 : duration;
	}

	boolean addDependency(Item item) {
		if (this.dependsOn.find(item.getId()) != null) {
			return false;
		}
		dependsOn.insertItem(item);
		return true;
	}

	boolean removeDependency(Item item) {
		if (this.dependsOn.find(item.getId()) == null) {
			return false;
		}
		dependsOn.remove(item);
		return true;
	}
	
	void removeDependency(String id) {
		dependsOn.remove(id);
	}

	int getEndPoint() {
		int endPoint = 0;
		ItemNode node = dependsOn.firstItemNode();
		
		while(node != null) {
			endPoint = node.item.getEndPoint() > endPoint ? node.item.getEndPoint() : endPoint;
			node = node.next;
		}
		if (node == null) {
			endPoint += duration;
		}
		return endPoint;
	}
	
	int getStartPoint() {
		return this.getEndPoint() - this.duration;
	}
	
	@Override
	public String toString() {
		return "Teilaufgabe " + id + " (Dauer: " + duration + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (duration == 0) {
			if (other.duration != 0)
				return false;
		} else if (!(duration == other.duration))
			return false;
		return true;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getDuration() {
		return duration;
	}
}
