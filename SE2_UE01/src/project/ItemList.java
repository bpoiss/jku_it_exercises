package project;

public class ItemList {
	private ItemNode head;

	ItemList() {
		head = null;
	}
	
	/** 
	 * Insert a new item. 
	 * @param id unique id of the item
	 * @param name
	 * @param duration
	 * @return true if item was created, false otherwise
	 */
	boolean insert(String id, String name, int duration) {
		return insertItem(new Item(id, name, duration)); 
	}
	
	boolean remove(String id) {
		return this.removeNode(id);
	}
	
	void remove(Item item) {
		String id = item.getId();
		this.removeNode(id);
	}

	/** 
	 * Inserts a new item at the end of the list. 
	 * @param item
	 * @return boolean true if item was inserted, false otherwise
	 */
	boolean insertItem(Item item) {
		ItemNode node = new ItemNode(item);
		
		if(this.find(item.getId()) != null) {
			return false;
		}

		if(head == null) {
			head = node;
		}
		else {
			ItemNode nodeIterator = head;
	
			while (nodeIterator.next != null) {
				nodeIterator = nodeIterator.next;
			}
			nodeIterator.next = node;
		}
		
		return true; 
	}

	/**
	 * @param id find ItemNode by item id
	 * @return ItemNode found, null if not found 
	 */
	ItemNode find(String id) {
		ItemNode node = head;

		while (node != null && !node.item.getId().equals(id)) {
			node = node.next;
		}

		if (node == null) {
			return null;
		}

		return node;
	}

	/**
	 * Returns the first ItemNode
	 * @return first ItemNode, null if empty
	 */
	ItemNode firstItemNode() {
		return head; 
	} 

	/** 
	 * Returns the next ItemNode for the given current node
	 * @param current the given current node
	 * @return next ItemNode
	 */
	ItemNode next(ItemNode current) {
		if (current == null || current.next == null) {
			return null; 
		}
		current = current.next; 
		return current.next; 
	} 

	/**
	 * @return true if list is empty, false otherwise
	 */
	boolean isEmpty() {
		return head == null; 
	}

	private boolean removeNode(String id) {
		boolean rc;
		
		if(this.find(id) != null) {
			rc = true;
			
			ItemNode node = firstItemNode();
			if(node != null) {
				while(node.next != null && node.next.item.getId() != id) {
					node = node.next;
				}
				if(node.next != null) {
					node.next = node.next.next != null ? node.next.next : null;
				}
			}
		}
		else {
			rc = false;
		}
		return rc;
	}
}
