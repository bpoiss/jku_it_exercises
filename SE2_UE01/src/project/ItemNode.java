package project;

public class ItemNode {
	Item item;
	ItemNode next;

	/**
	 * @param item the first item in the list
	 */
	ItemNode(Item item) {
		this.item = item;
	}

}
