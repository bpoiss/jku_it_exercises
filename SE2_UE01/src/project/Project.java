package project;

public class Project {
	private final ItemList list; 
	
	public Project() {
		list = new ItemList(); 
	}
	
	/** 
	 * Insert a new item. If the item already exists,
	 * nothing is done.
	 * @param id unique id of the item
	 * @param name
	 * @param duration
	 * @return true if item was created, false otherwise
	 */
	public boolean addItem(String id, String name, int duration) {
		return insertItem(new Item(id, name, duration)); 
	}
	
	/** 
	 * Removes item from the list. If the item doesn't exist,
	 * nothing is done.
	 * @param id unique id of the item
	 * @return true if item was removed, false otherwise
	 */
	public boolean removeItem(String id) {
		boolean rc;
		rc = list.remove(id);
		
		// Delete all dependencies where an item depends on the deleted item
		if(rc) {
			ItemNode node = list.firstItemNode();
	
			while(node != null) {
				node.item.removeDependency(id);
				node = node.next;
			}
		}
		
		return rc;
	}

	/** 
	 * Adds a dependency between items. If the dependency already exists,
	 * nothing is done.
	 * @param itemId id of the item from that the other item depends
	 * @param itemDependendId id of the item that depends on the item with itemId
	 * @return true if dependency was added, false otherwise
	 */
	public boolean addDependency(String itemId, String itemDependendId) {
		ItemNode itemNode = list.find(itemDependendId);
		if(itemNode == null || itemNode.item == null) {
			return false;
		}
		Item item = itemNode.item;
		return item.addDependency(list.find(itemId).item);
	}

	/** 
	 * Removes a dependency between items. If the dependency doesn't exist,
	 * nothing is done.
	 * @param itemId id of the item from that the other item depends
	 * @param itemDependendId id of the item that depends on the item with itemId
	 * @return true if dependency was removed, false otherwise
	 */
	public boolean removeDependency(String itemId, String itemDependendId) {
		Item item = list.find(itemDependendId).item;
		return item.removeDependency(list.find(itemId).item);
	}
	
	/** 
	 * @return Endpoint of an item from the Project, -1 if item not exists
	 */
	public int getItemEndPoint(String id) {
		ItemNode itemNode = list.find(id);
		if(itemNode != null) {
			Item item = itemNode.item;
			return item.getEndPoint();
		}
		else {
			return -1;
		}
	}
	
	/** 
	 * @return Startpoint of an item from the Project, -1 if item not exists
	 */
	public int getItemStartPoint(String id) {
		ItemNode itemNode = list.find(id);
		if(itemNode != null) {
			Item item = itemNode.item;
			return item.getStartPoint();
		}
		else {
			return -1;
		}
	}
	
	/** 
	 * @return Endpoint of the whole project
	 */
	public int getDuration() {
		int endpoint = 0;
		ItemNode itemNode = this.list.firstItemNode();

		while(itemNode != null) {
			endpoint = itemNode.item.getEndPoint() > endpoint ? itemNode.item.getEndPoint() : endpoint;
			itemNode = itemNode.next;
		}
		
		return endpoint;
	}
	
	/** 
	 * @param time moment to select all items that end here
	 * @return ItemList of all items that end at a moment
	 */
	public Item[] getItemsByStart(int time) {
		int i = 0, j = 0;
		ItemNode itemNode = this.list.firstItemNode();
		
		// calculate array size
		while(itemNode != null) {
			if(itemNode.item.getStartPoint() == time) {
				i++;
			}
			itemNode = itemNode.next;
		}

		itemNode = this.list.firstItemNode();
		Item[] items = new Item[i];
		
		// fill array
		while(itemNode != null) {
			if(itemNode.item.getStartPoint() == time) {
				items[j] = itemNode.item;
				j++;
			}
			itemNode = itemNode.next;
		}
		
		return items;
	}
	
	/** 
	 * @param time moment to select all items that end here
	 * @return ItemList of all items that end at a moment
	 */
	public Item[] getItemsByEnd(int time) {
		int i = 0, j = 0;
		ItemNode itemNode = this.list.firstItemNode();
		
		// calculate array size
		while(itemNode != null) {
			if(itemNode.item.getEndPoint() == time) {
				i++;
			}
			itemNode = itemNode.next;
		}

		itemNode = this.list.firstItemNode();
		Item[] items = new Item[i];
		
		// fill array
		while(itemNode != null) {
			if(itemNode.item.getEndPoint() == time) {
				items[j] = itemNode.item;
				j++;
			}
			itemNode = itemNode.next;
		}
		
		return items;
	}
	
	public boolean containsEntry(String id) {
		return list.find(id) != null; 
	}
	
	// - private section ------------------------------------------------------
	
	private boolean insertItem(Item item) {
		return list.insertItem(item); 
	}

}
