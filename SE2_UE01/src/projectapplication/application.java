package projectapplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import project.*;

public class application {

	private static Project project = new Project();

	public static void main(String[] args) {
		String line = "";
		printMenu();
		while (!line.equalsIgnoreCase("H")) {			
			if (line != "") {
				processCommand(line);
			}
			System.out.println();
			System.out.print("command: ");
			line = readLine();
		}
	}

	private static void printMenu() {
		System.out.println("A: add project item");
		System.out.println("B: remove project item");
		System.out.println("C: add dependency");
		System.out.println("D: remove dependency");
		System.out.println("E: get earliest possible start time of item");
		System.out.println("F: print project calendar");
		System.out.println("G: get project duration");
		System.out.println("H: quit");
	}

	private static String readLine() {
		String line = "";
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));

		try {
			line = input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}

	private static void processCommand(String line) {
		switch (line.toUpperCase()) {
		case "A":
			addProjectItem();
			break;
		case "B":
			removeProjectItem();
			break;
		case "C":
			addDependency();
			break;
		case "D":
			removeDependency();
			break;
		case "E":
			getItemStartTime();
			break;
		case "F":
			printCalendar();
			break;
		case "G":
			getProjectDuration();
			break;
		case "H":
			break;
		default:
			System.out.println("Not a command");
		}
	}

	private static void addProjectItem() {
		System.out.print("item id: ");
		String id = readLine();
		System.out.print("name: ");
		String name = readLine();
		System.out.print("duration: ");
		int duration = Integer.parseInt(readLine());

		System.out.println(project.addItem(id, name, duration) ? "item successfully added"
						: "item already exists");
	}

	private static void removeProjectItem() {
		System.out.print("item id: ");
		String id = readLine();

		System.out.println(project.removeItem(id) ? "item successfully removed\n"
						: "item doesn't exists\n");
	}

	private static void addDependency() {
		System.out.print("before-item id: ");
		String beforeId = readLine();
		System.out.print("after-item id: ");
		String afterId = readLine();

		System.out.println(project.addDependency(beforeId, afterId) ? "dependency successfully added"
						: "dependency already exists");
	}

	private static void removeDependency() {
		System.out.print("before-item id: ");
		String beforeId = readLine();
		System.out.print("after-item id: ");
		String afterId = readLine();

		System.out.println(project.removeDependency(beforeId, afterId) ? "dependency successfully removed"
						: "dependency didn't exists");
	}

	private static void getItemStartTime() {
		System.out.print("item id: ");
		String id = readLine();
		
		System.out.println("earliest possible start time: " + project.getItemStartPoint(id));
	}

	private static void printCalendar() {
		int time = 0;
		int endTime = project.getDuration();
		
		while(time <= endTime) {
			Item[] starts = project.getItemsByStart(time);
			Item[] ends = project.getItemsByEnd(time);
			for (Item item : ends) {
				System.out.println(time + ": end " + item.getName() + " (" + item.getDuration() + ")");
			}
			for (Item item : starts) {
				System.out.println(time + ": start " + item.getName() + " (" + item.getDuration() + ")");
			}
			time++;
		}
	}

	private static void getProjectDuration() {
		System.out.println("project duration  " + project.getDuration());
	}
}
