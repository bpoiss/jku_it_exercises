package projecttest;

import static org.junit.Assert.*;
import org.junit.Test;

import project.Item;
import project.Project;

public class ProjectTest {
	
	@Test
	public void testGetItemEndPoint() {
		int actual;
		Project project = initExampleProject();
		
		actual = project.getItemEndPoint("D");
		
		assertEquals(14, actual);
		
	}
	
	@Test
	public void testGetItemStartPoint() {
		int actual;
		Project project = initExampleProject();
		
		actual = project.getItemStartPoint("D");
		
		assertEquals(12, actual);
		
	}
	
	@Test
	public void testRemoveDependency() {
		int actual;
		Project project = initExampleProject();

		project.removeDependency("B", "C");
		
		actual = project.getItemStartPoint("D");
		
		assertEquals(10, actual);
		
	}
	
	@Test
	public void testRemoveItem() {
		boolean actual;
		Project project = initExampleProject();

		project.removeItem("C");
		actual = project.containsEntry("C");
		
		assertEquals(false, actual);
	}
	
	@Test
	public void testRemoveItemWithDependency() {
		int actual;
		Project project = initExampleProject();

		project.removeItem("B");
		
		actual = project.getItemStartPoint("D");
		
		assertEquals(10, actual);
	}
	
	@Test
	public void testGetEndpoint() {
		int actual;
		Project project = initExampleProject();
		actual = project.getDuration();
		
		assertEquals(14, actual);
	}
	
	@Test
	public void testGetItemByStart() {
		int actual;
		Project project = initExampleProject();
		project.addItem("E", "Erneuerungsphase", 3);
		project.addDependency("B", "E");
		
		Item[] items = project.getItemsByStart(7);
		
		actual = items.length;
		
		assertEquals(2, actual);
	}
	
	@Test
	public void testGetItemByEnd() {
		int actual;
		Project project = initExampleProject();
		project.addItem("E", "Erneuerungsphase", 7);
		
		Item[] items = project.getItemsByEnd(7);
		
		actual = items.length;
		
		assertEquals(2, actual);
	}
	
	private Project initExampleProject() {
		Project project = new Project();
		
		project.addItem("A", "Planungsphase", 5);
		project.addItem("B", "Materialbesorgung", 7);
		project.addItem("C", "Bauphase", 5);
		project.addItem("D", "Testphase", 2);

		project.addDependency("A", "C");
		project.addDependency("B", "C");
		project.addDependency("C", "D");
		
		return project;
	}

}
