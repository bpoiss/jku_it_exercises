package Uebung2;

import simu.Resource;

public class AnnouncementDesk extends Resource {
	private static final long CHECKIN_MIN = 4;
	private static final long CHECKIN_MAX = 8;
	private static final long CHECKOUT_MIN = 2;
	private static final long CHECKOUT_MAX = 4;

	protected AnnouncementDesk(String name) {
		super(name);
	}
	
	public void checkin() {
		delay(rand(CHECKIN_MIN, CHECKIN_MAX), "checking in"); 
	}
	
	public void checkout() {
		delay(rand(CHECKOUT_MIN, CHECKOUT_MAX), "checking out"); 
	}

}
