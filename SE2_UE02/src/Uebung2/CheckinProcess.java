package Uebung2;

import simu.Process;

public class CheckinProcess extends Process {	
	protected CheckinProcess(String name) {
		super(name);
	}

	@Override
	protected void execute() {
		AnnouncementDesk announcementDesk = request(AnnouncementDesk.class);
		announcementDesk.checkin();
		release(announcementDesk);
	}
}