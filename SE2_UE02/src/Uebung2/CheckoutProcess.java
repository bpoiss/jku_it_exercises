package Uebung2;

import simu.Process;

public class CheckoutProcess extends Process {	
	protected CheckoutProcess(String name) {
		super(name);
	}

	@Override
	protected void execute() {
		AnnouncementDesk announcementDesk = request(AnnouncementDesk.class);
		announcementDesk.checkout();
		release(announcementDesk);
	}
}