package Uebung2;

public class InspectionProcess extends CheckinProcess {
	protected InspectionProcess(String name) {
		super(name);
	}

	@Override
	protected void execute() {
		super.execute();
		
		MedicalAssistant medicalAssistant = request(MedicalAssistant.class);
		medicalAssistant.inspect();
		release(medicalAssistant);
	}

}