package Uebung2;

import simu.Simulation;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Simulation simu = Simulation.getInstance();

		simu.addResource(new AnnouncementDesk("Anmeldeschalter 1"));
		simu.addResource(new MedicalDirector("Dr. Wagner"));
		simu.addResource(new MedicalAssistant("Assistenzarzt M�ller"));
		simu.addResource(new MedicalAssistant("Assistenzarzt Prager"));
		simu.addResource(new MedicalAssistant("Assistenzarzt Gutrun"));
		simu.addResource(new Nurse("Schwester Simone"));
		simu.addResource(new Nurse("Schwester Hermine"));

		simu.scheduleProcessAt(new CheckinProcess("Checkin 1"), 0);
		simu.scheduleProcessAt(new CheckinProcess("Checkin 2"), 0);
		simu.scheduleProcessAt(new SurgicalInterventionProcess("Surgical Intervention 1"), 1);
		simu.scheduleProcessAt(new SurgicalInterventionProcess("Surgical Intervention 2"), 10);
		simu.scheduleProcessAt(new CheckoutProcess("Checkout 1"), 15);
		simu.scheduleProcessAt(new InspectionProcess("Inspection 1"), 15);
		simu.scheduleProcessAt(new MedicationPickupProcess("Pickup 1"), 16);
		simu.scheduleProcessAt(new CheckoutProcess("Checkout 2"), 20);
		simu.scheduleProcessAt(new InspectionProcess("Inspection 2"), 15);
		
		simu.setTimeFactor(5);
		
		simu.start();
	}

}
