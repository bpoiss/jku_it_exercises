package Uebung2;

import simu.Resource;

public class MedicalAssistant extends Resource {
	private static final long INSPECTTIME_MIN = 10;
	private static final long INSPECTTIME_MAX = 40;
	private static final long SURGEYTIME_MIN = 60;
	private static final long SURGEYTIME_MAX = 120;

	protected MedicalAssistant(String name) {
		super(name);
	}
	
	public void inspect() {		
		delay(rand(INSPECTTIME_MIN, INSPECTTIME_MAX), "inspecting"); 		
	}

	public void surgey() {
		delay(rand(SURGEYTIME_MIN, SURGEYTIME_MAX), "surgeying"); 	
	}
}
