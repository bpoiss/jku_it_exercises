package Uebung2;

import simu.Resource;

public class MedicalDirector extends MedicalAssistant {
	private static final long WORKTIME_MIN = 8;
	private static final long WORKTIME_MAX = 15;

	protected MedicalDirector(String name) {
		super(name);
	}
	
	public void check() {		
		delay(rand(WORKTIME_MIN, WORKTIME_MAX), "checking"); 		
	}

}
