package Uebung2;

import simu.Process;

public class MedicationPickupProcess extends Process {

	protected MedicationPickupProcess(String name) {
		super(name);
	}

	@Override
	protected void execute() {
		Nurse nurse = request(Nurse.class);
		nurse.pickup();
		release(nurse);
	}
}