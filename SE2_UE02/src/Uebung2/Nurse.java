package Uebung2;

import simu.Resource;

public class Nurse extends Resource {

	private static final long PICKUP_MIN = 4;
	private static final long PICKUP_MAX = 12;

	protected Nurse(String name) {
		super(name);
	}
	
	public void pickup() {		
		delay(rand(PICKUP_MIN, PICKUP_MAX), "picking up medication"); 		
	}

}
