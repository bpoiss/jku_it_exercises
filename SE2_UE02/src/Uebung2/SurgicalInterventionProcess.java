package Uebung2;

public class SurgicalInterventionProcess extends InspectionProcess {

	protected SurgicalInterventionProcess(String name) {
		super(name);
	}

	@Override
	protected void execute() {
		super.execute();

		MedicalDirector medicalDirector = request(MedicalDirector.class);
		medicalDirector.check();
		Nurse nurse = request(Nurse.class);
		
		if(!isAvailable(MedicalAssistant.class)) {
			medicalDirector.surgey();
		}
		else {
			MedicalAssistant medicalAssistant = request(MedicalAssistant.class);
			medicalAssistant.surgey();
			release(medicalAssistant);
		}
		medicalDirector.check();
		release(medicalDirector);
		release(nurse);
	}

}