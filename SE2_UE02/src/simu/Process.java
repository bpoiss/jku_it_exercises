package simu;

/**
 * Base class for implementing simulation processes. 
 * Derive from this class to implement our own processes. 
 * Subclasses should implement their specific process 
 * in method {@link #execute}. 
 * 
 * @author hp
 *
 */
public abstract class Process extends SimuBase {
	
	// API -------------------------------------------------------------------

	/**
	 * Constructor creating a process with given name. 
	 * Call this constructor in constructor of subclass. 
	 * @param name the name of the process 
	 */
	protected Process(String name) {
		super(name);
	}

	/** 
	 * Executes this process. 
	 * Subclasses should implement their specific process 
	 * by implementing this method. 
	 */
	protected abstract void execute();

	/** Returns if this process is finished.
	 * @return true if process is finished
	 */
	public boolean isFinished() {
		return finished; 
	}
	
	// ------------------------------------------------------------------------

	/** The time when the process is started */
	private long startTime;
	
	/** The time needed for processing the process */
	private long processingTime;
	
	/** The time waiting for resources */
	private long waitingTime;
	
	/** Flag indicating that this process has finished */
	private boolean finished; 

	/**
	 * Starts this process. Each process is started in its own thread of execution. 
	 * Calls {@link #execute}. 
	 */
	void start() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				printTrace("started");
				startTime = Simulation.getInstance().getTime();
				execute();
				printTrace("finished");
				processingTime = Simulation.getInstance().getTime() - startTime;
				waitingTime = getWaitTime(Thread.currentThread());
				finished = true; 
			}

		}).start();

	}
	

	/**
	 * Prints a statistic about this process. 
	 * Prints when the process has started and what the overall processing time was. 
	 */
	void printStatistics() {
		printMessage(String.format("Process %s: issued at %d, processing time = %d, waiting time = %d",
				getName(), startTime, processingTime, waitingTime));
	}

}
