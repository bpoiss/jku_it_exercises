package simu;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Base class for resources. 
 * A resource is an object in the simulation which is needed by 
 * a process and can be occupied. 
 * 
 * Resources therefore can be requested and when available will be 
 * occupied and then should be released. 
 * 
 * @author hp
 *
 */
public abstract class Resource extends SimuBase {

	// API -------------------------------------------------------------------
	
	/**
	 * Constructor initializing name. Call this constructor in constructor of 
	 * subclass. 
	 * 
	 * @param name the name of this resource 
	 */
	protected Resource(String name) {
		super(name);
	}

	/**
	 * Returns if this resource object is occupied currently 
	 * @return true if this resource object is occupied currently
	 */
	public boolean isOccupied() {
		return occupied;
	}

	// ------------------------------------------------------------------------


	/**
	 * Requests a resource of the given type. 
	 * Will wait until the resource is available. 
	 * Will return the resource object occupied. 
	 * 
	 * @param resourceType the class object representing the resource type requested
	 * @return the resource object occupied
	 * 
	 */
	static final <R extends Resource> R requestResource(Class<R> resourceType) {
		synchronized (pool) {
			long requestTime = Simulation.getInstance().getTime();
			setRequestTime(resourceType, requestTime); 
			R free = (R) getFreeResource(resourceType);
			if (free == null) {
				SimuBase.printStaticTrace(String.format(
						"tried requesting %s ", resourceType.getName()));
			}
			while (free == null) {
				try {
					pool.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				free = (R) getFreeResource(resourceType);
			}
			pool.remove(free);
			free.printTrace("occupied");
			long current = Simulation.getInstance().getTime();
			free.occupyTime = current;
			free.timeRequested += current - getRequestTime(resourceType);
			free.timeFree += current - free.releaseTime;
			free.occupied = true;
			incrWaitTime(Thread.currentThread(), current - requestTime);
			return (R) free;
		}

	}

	/**
	 * Releases this resource object. 
	 */
	final void release() {
		synchronized (pool) {
			releaseTime = Simulation.getInstance().getTime();
			timeOccupied += releaseTime - occupyTime;
			pool.add(this);
			occupied = false;
			printTrace("released");
			pool.notifyAll();
		}
	}

	/**
	 * Checks if a resource of the given type is available. 
	 * 
	 * @param resourceType the class object representing the resource type requested
	 * @return the resource object occupied
	 * 
	 */
	static final <R extends Resource> boolean isResourceAvailable(Class<R> resourceType) {
		return getFreeResource(resourceType) != null;
	}

	/** the total time this resource is not occupied */
	long timeFree;
	/** the total time this resource is occupied */
	long timeOccupied;
	/** the total time this resource was requested before becoming available */
	long timeRequested;
	/** the last time this resource object was released */
	long releaseTime;
	/** the last time this resource object was occupied */
	long occupyTime;

	/** flag indicating that this resource object is occupied */
	boolean occupied;

	/** Set with all the resource objects currently available */
	private static Set<Resource> pool = new HashSet<>(); 

	/**
	 * Gets the next resource object of given type from the resource pool. 
	 * Returns null if no resource of given type is available.  
	 * @param resourceType the class object representing the resource type requested 
	 * @return the next free resource object of given type, null if none available 
	 */
	private static <R extends Resource> R getFreeResource(Class<R> resourceType) {
		for (Resource r : pool) {
			if (r.isResourceType(resourceType)) {
				return (R) r;
			}
		}
		return null;
	}

	/**
	 * Checks if this resource type is equal type or a subtype of the given type
	 * 
	 * @param resourceType the resource type 
	 * @return true if type of this resource type is equal type or a subtype of the given type 
	 */
	private boolean isResourceType(Class<? extends Resource> resourceType) {
		return isSubTypeOf(resourceType);
	}

	/**
	 * Checks if this class is a subtype of the given resource type
	 * @param resourceType the resource type
	 * @return true if this class is a subtype of resource type
	 */
	private boolean isSubTypeOf(Class<? extends Resource> resourceType) {
		Class<?> clazz = this.getClass(); 
		while (clazz != Resource.class) {
			if (resourceType == clazz) {
				return true;
			}
			clazz = clazz.getSuperclass(); 
		}
		return false;
	}

	/**
	 * Prints a statistics about this resource. 
	 * Prints the total times this resource was occupied, free, and requested. 
	 */
	protected void printStatistics() {
		long current = Simulation.getInstance().getTime();
		if (isOccupied()) {
			timeOccupied += current - occupyTime;
		} else {
			timeFree += current - releaseTime;
		}
		printMessage(
				String.format("Resource %s: time occupied = %d, time idle = %d, time requested = %d ",
						getName(), timeOccupied, timeFree, timeRequested));
	}

}
