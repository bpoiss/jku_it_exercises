package simu;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Base class of simulation objects. This class is package internal and serves
 * as a common base class for Process and Resource. Provides a random number
 * generator and a name property. Provides methods for printing messages for
 * derived classes. Provides a method for delay an activity some time.
 * 
 * Use methods for implementing your processes
 * <ul>
 * <li> {@link #delay} for delaying execution 
 * <li> {@link #rand} for generating uniformally distributed random numbers 
 * <li> {@link #request} for requesting a resource of specific type
 * <li> {@link #release} for releasing a resource object
 * <li> {@link #isAvailable} for testing if a resource of specific type is available
 * </ul>
 * 

 * @author hp
 * 
 */
abstract class SimuBase {

	// API -------------------------------------------------------------------

	/**
	 * Gets the name of this simulation object.
	 * 
	 * @return the name of this simulation object
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Simulates a delay of this process doing some activity. 
	 * Delays execution for given time. 
	 *  
	 * @param time the delay time
	 * @param activity the activity done while waiting
	 */
	protected final void delay(long time, String activity) {
		long currentTime = Simulation.getInstance().getTime();
		printTrace(String.format("%d time units delay for %s", time, activity));
		delay(time);
		Simulation.getInstance().advanceTimeTo(currentTime + time);
		printTrace(String.format("finished %s", activity));
	}


	/**
	 * Generates a random number between min and max. Use this methods to
	 * generate uniformally distributed delay times.
	 * 
	 * @param min
	 *            the lower bound of the generated values
	 * @param max
	 *            the upper bound of the generated values
	 * @return random number between min and max
	 */
	protected final long rand(long min, long max) {
		long delta = max - min;
		return min + (long) (randGen.nextDouble() * delta);
	}

	/**
	 * Requests a resource of the given type. 
	 * Will wait until the resource is available. 
	 * Will return the resource object occupied. 
	 * 
	 * @param resourceType the class object representing the resource type requested
	 * @return the resource object occupied
	 */
	protected final <R extends Resource> R request(Class<R> resourceType) {
		return Resource.requestResource(resourceType);
	}

	/**
	 * Releases the given resource object. 
	 * 
	 * @param resource the resource object released. 
	 */
	protected final void release(Resource resource) {
		resource.release();
	}
	/**
	 * Prints a message string on the console.
	 * 
	 * @param msg
	 *            the message string to print
	 */
	
	/**
	 * Checks if a resource of the given type is available. 
	 * @param resourceType the class object representing the resource type requested
	 * @return true if a resource of given type is available 
	 */
	protected final <R extends Resource> boolean isAvailable(Class<R> resourceType) {
		return Resource.isResourceAvailable(resourceType);
	}
	
	protected void printMessage(String msg) {
		System.out.println(msg);
	}

	/**
	 * Prints a trace message on console. Prints first the simulation time, then
	 * the name of this object and then the message string
	 * 
	 * @param msg
	 *            the message string to print
	 */
	protected void printTrace(String msg) {
		System.out.format("TIME %5d: %s %s %n", Simulation.getInstance()
				.getTime(), getName(), msg);
	}

	/**
	 * Prints a trace message on console from a static context. Prints first the
	 * simulation time and then the message string
	 * 
	 * @param msg
	 *            the message string to print
	 */
	protected static void printStaticTrace(String msg) {
		System.out.format("TIME %5d: %s %n",
				Simulation.getInstance().getTime(), msg);
	}

	// ------------------------------------------------------------------------

	/** Random number seed */
	private final Random randGen = new Random();

	/** Name field */
	private final String name;

	/**
	 * Constructor initializing name.
	 * 
	 * @param name
	 *            the name of the simulation object
	 */
	SimuBase(String name) {
		super();
		this.name = name;
	}

	/**
	 * Delays the current process execution for some time.
	 * 
	 * @param duration
	 *            the time to delay the current process execution.
	 */
	final void delay(long duration) {
		if (duration <= 0)
			return;
		try {
			Thread.sleep(duration * 1000
					/ Simulation.getInstance().getTimeFactor());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// maintaining wait times for processes  
	
	/** map for storing wait times for process threads */
	static Map<Thread, Long> waitTimes = new HashMap<>(); 
	
	/**
	 * Gets the wait time for the thread
	 * @param thread
	 * @return the wait time for the thread
	 */
	static long getWaitTime(Thread thread) {
		Long time = waitTimes.get(thread);
		if (time == null) {
			return 0; 
		} else {
			return time.longValue(); 
		}
	}
	
	/**
	 * Incr the wait time for the thread. Threads are equal to processes
	 * @param thread the thread object to identify the process 
	 * @param time wait time for the thread
	 */
	static void incrWaitTime(Thread thread, long time) {
		waitTimes.put(thread, getWaitTime(thread) + time);
	}

	// maitaining request times for resources 
	
	/** map for storing request times for resources types */
	static Map<Class, Long> requestTimes = new HashMap<>(); 
	
	/**
	 * Gets the request time for this resource type
	 * @param rClazz
	 * @return time the resource of type rClazz has been requested
	 */
	static long getRequestTime(Class rClazz) {
		Long time = requestTimes.get(rClazz);
		if (time == null) {
			return 0; 
		} else {
			return time.longValue(); 
		}
	}
	
	/**
	 * Sets the request time for the resource type
	 * @param rClazz
	 * @param time the resource of type rClazz has been requested
	 */
	static void setRequestTime(Class rClazz, long time) {
		requestTimes.put(rClazz, time);
	}
	
}
