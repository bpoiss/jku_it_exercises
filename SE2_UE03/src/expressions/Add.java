package expressions;

public class Add extends Operation{
	public Add(Expression left, Expression right) {
		super(left, right);
	}
	
	protected String name() {
		return "+";
	}
	
	@Override
	public double evaluate() throws ArithmeticException {
		return left.evaluate() + right.evaluate();
	}
	
	@Override
	public Expression simplify() {
		Expression leftSimpl = left.simplify();
		Expression rightSimpl = right.simplify();
		if(leftSimpl.isZero()) {
			return rightSimpl;
		}
		else if(rightSimpl.isZero()) {
			return leftSimpl;
		}
		else if(leftSimpl.isEquivTo(rightSimpl)) {
			return new Mult(new Constant(2.0), leftSimpl);
		}
		else {
			return new Add(leftSimpl, rightSimpl);
		}
	}
	
	@Override
	protected boolean isEquivTo(Expression other) {
		if(other instanceof Add) {
			Add that = (Add)other;
			return (this.left.isEquivTo(that.left) && 
					this.right.isEquivTo(that.right)) ||
					(this.left.isEquivTo(that.right) && 
					this.right.isEquivTo(that.left));
		}
		return false;
	}
}
