package expressions;

public class Constant extends Expression {
	protected double value;
	
	public Constant(double value) {
		this.value = value;
	}

	public double getValue() {
		return this.value;
	}

	@Override
	protected String toInfixString() {
		return String.valueOf(value);
	}

	@Override
	protected Expression simplify() {
		return this;
	}

	@Override
	protected boolean isEquivTo(Expression expression) {
		if(expression instanceof Constant) {
			return ((Constant)expression).value == this.value ? true : false;
		}
		return false;
	}

	@Override
	protected double evaluate() {
		return this.value;
	}
}
