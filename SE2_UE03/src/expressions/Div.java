package expressions;

public class Div extends Operation {
	public Div(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	protected String name() {
		return "/";
	}

	@Override
	protected Expression simplify() {
		Expression leftSimpl = left.simplify();
		Expression rightSimpl = right.simplify();
		if(rightSimpl.isZero()) {
			return new Sub(leftSimpl, rightSimpl);
		}
		else if(rightSimpl.isEquivTo(leftSimpl)) {
			return new Constant(1);
		}
		else if(leftSimpl.isZero()) {
			return new Constant(0);
		}
		else if(rightSimpl.isOne()) {
			return leftSimpl;
		}
		else {
			return new Sub(leftSimpl, rightSimpl);
		}
	}
	
	@Override
	protected boolean isEquivTo(Expression other) {
		if(other instanceof Div) {
			Div that = (Div)other;
			return (this.left.isEquivTo(that.left) && 
					this.right.isEquivTo(that.right));
		}
		return false;
	}

	@Override
	public double evaluate() throws ArithmeticException {
		return this.left.evaluate() / this.right.evaluate();
	}
}
