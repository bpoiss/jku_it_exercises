package expressions;

public abstract class Expression {

	/**
	 * Simplifies the Expression
	 * @return simplified Expression
	 */
	protected abstract Expression simplify();
	/**
	 * @return A String representation of the Expression
	 */
	protected abstract String toInfixString();
	/**
	 * Checks if the Expression is equal to the param expression
	 * @param expression The expression to check
	 * @return true, if the expressions are equal, false otherwise
	 */
	protected abstract boolean isEquivTo(Expression expression);
	/**
	 * Calculates the double value of the expression
	 * @return The double value of the expression
	 */
	protected abstract double evaluate();

	/**
	 * @return true, if the expression is a Constant with value 0, false otherwise
	 */
	protected boolean isZero() {
		return this instanceof Constant && ((Constant) this).getValue() == 0 ? true : false;
	}
	/**
	 * @return true, if the expression is a Constant with value 1, false otherwise
	 */
	protected boolean isOne() {
		return this instanceof Constant && ((Constant) this).getValue() == 1 ? true : false;
	}
	
}
