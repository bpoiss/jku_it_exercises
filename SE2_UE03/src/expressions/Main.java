package expressions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
	
	public static ArrayList<Expression> EXPRESSIONS = new ArrayList<Expression>();

	public static void main(String[] args) {
		String line = "";
		printMenu();
		while (!line.equalsIgnoreCase("q")) {			
			if (line != "") {
				processCommand(line);
			}
			System.out.println();
			System.out.print("command: ");
			line = readLine();
		}
	}

	private static void printMenu() {
		System.out.println("Expression Calculator Commands");
		System.out.println("==============================");
		System.out.println("c - new constant");
		System.out.println("v - new variable");
		System.out.println("+ - new add expression");
		System.out.println("- - new sub expression");
		System.out.println("* - new mult expression");
		System.out.println("/ - new div expression");
		System.out.println("a - assign value to variable");
		System.out.println("e - evaluate expression");
		System.out.println("s - simplify expression");
		System.out.println("q - quit");
		System.out.println("==============================");
	}

	private static String readLine() {
		String line = "";
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));

		try {
			line = input.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return line;
	}

	private static void processCommand(String line) {
		switch (line.toLowerCase()) {
		case "c":
			newConst();
			break;
		case "v":
			newVariable();
			break;
		case "+":
			newAdd();
			break;
		case "-":
			newSub();
			break;
		case "*":
			newMult();
			break;
		case "/":
			newDiv();
			break;
		case "a":
			assignValue();
			break;
		case "e":
			evaluate();
			break;
		case "s":
			newSimplify();
			break;
		case "q":
			System.out.println("Programm exit");
			break;
		default:
			System.out.println("Not a command");
		}
	}

	private static void evaluate() {
		System.out.print("Expression index: ");
		Expression exp = null;
		
		try {
			exp = EXPRESSIONS.get(Integer.valueOf(readLine()));
			System.out.println("Result: " + exp.evaluate());
		}
		catch(IndexOutOfBoundsException exception) {
			System.out.println("Expression doesn't exist");
		}
	}

	private static void assignValue() {
		System.out.print("Variable name: ");
		String name = readLine();
		Variable var = null;
		for(Expression exp : EXPRESSIONS) {
			if((exp instanceof Variable) && ((Variable)exp).name.equals(name)) {
				var = (Variable) exp;
			}
		}
		if(var == null) {
			System.out.println("Variable not found");
		}
		else {
			System.out.print("Value: ");
			var.value = Double.parseDouble(readLine());
		}
	}

	private static void newSimplify() {
		System.out.print("Expression index for simplification: ");
		Expression exp = EXPRESSIONS.get(Integer.parseInt(readLine())).simplify();
		System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + exp.toInfixString());
		EXPRESSIONS.add(exp);
	}

	private static void newDiv() {
		System.out.print("Expression index for left: ");
		int leftIndex = Integer.parseInt(readLine());
		System.out.print("Expression index for right: ");
		int rightIndex = Integer.parseInt(readLine());
		Div newDiv = new Div(EXPRESSIONS.get(leftIndex), EXPRESSIONS.get(rightIndex));
		System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + newDiv.toInfixString());
		EXPRESSIONS.add(newDiv);
	}

	private static void newMult() {
		System.out.print("Expression index for left: ");
		int leftIndex = Integer.parseInt(readLine());
		System.out.print("Expression index for right: ");
		int rightIndex = Integer.parseInt(readLine());
		Mult newMult = new Mult(EXPRESSIONS.get(leftIndex), EXPRESSIONS.get(rightIndex));
		System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + newMult.toInfixString());
		EXPRESSIONS.add(newMult);
	}

	private static void newSub() {
		System.out.print("Expression index for left: ");
		int leftIndex = Integer.parseInt(readLine());
		System.out.print("Expression index for right: ");
		int rightIndex = Integer.parseInt(readLine());
		Sub newSub = new Sub(EXPRESSIONS.get(leftIndex), EXPRESSIONS.get(rightIndex));
		System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + newSub.toInfixString());
		EXPRESSIONS.add(newSub);
	}

	private static void newAdd() {
		System.out.print("Expression index for left: ");
		int leftIndex = Integer.parseInt(readLine());
		System.out.print("Expression index for right: ");
		int rightIndex = Integer.parseInt(readLine());
		Add newAdd = new Add(EXPRESSIONS.get(leftIndex), EXPRESSIONS.get(rightIndex));
		System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + newAdd.toInfixString());
		EXPRESSIONS.add(newAdd);
	}

	private static void newVariable() {
		System.out.print("Variable value: ");
		String var = readLine();
		boolean variableExists = false;
		
		for(Expression exp : EXPRESSIONS) {
			if((exp instanceof Variable) && ((Variable)exp).name.equals(var)) {
				System.out.println("Variable already exists.");
				variableExists = true;
			}
		}
		if (!variableExists) {
			System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + var);
			EXPRESSIONS.add(new Variable(var));
		}
	}

	private static void newConst() {
		System.out.print("Const value: ");
		double value = Double.parseDouble(readLine());
		System.out.println("  [" + EXPRESSIONS.size() + "]:" + " " + value);
		EXPRESSIONS.add(new Constant(value));
	}
}
