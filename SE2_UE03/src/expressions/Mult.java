package expressions;

public class Mult extends Operation {
	public Mult(Expression left, Expression right) {
		super(left, right);
	}
	
	protected String name() {
		return "*";
	}
	
	@Override
	public double evaluate() throws ArithmeticException {
		return left.evaluate() * right.evaluate();
	}
	
	@Override
	public Expression simplify() {
		Expression leftSimpl = left.simplify();
		Expression rightSimpl = right.simplify();
		if(leftSimpl.isZero() || rightSimpl.isZero()) {
			return new Constant(0.0);
		}
		else if(leftSimpl.isOne()) {
			return rightSimpl;
		}
		else if (rightSimpl.isOne()) {
			return leftSimpl;
		}
		else {
			return new Mult(leftSimpl, rightSimpl);
		}
	}

	@Override
	protected boolean isEquivTo(Expression other) {
		if(other instanceof Mult) {
			Mult that = (Mult)other;
			return (this.left.isEquivTo(that.left) && 
					this.right.isEquivTo(that.right)) ||
					(this.left.isEquivTo(that.right) && 
					this.right.isEquivTo(that.left));
		}
		return false;
	}
}
