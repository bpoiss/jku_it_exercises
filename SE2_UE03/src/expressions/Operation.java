package expressions;

public abstract class Operation extends Expression {
	protected Expression left, right;
	
	public abstract double evaluate() throws ArithmeticException;	
	protected abstract String name();
	
	public Operation(Expression left, Expression right) {
		this.left = left;
		this.right = right;
	}


	@Override
	protected String toInfixString() {
		return "(" + left.toInfixString() + " " + this.name() + " " + right.toInfixString() + ")";
	}

}
