package expressions;

public class Sub extends Operation {
	public Sub(Expression left, Expression right) {
		super(left, right);
	}

	@Override
	protected String name() {
		return "-";
	}

	@Override
	protected Expression simplify() {
		Expression leftSimpl = left.simplify();
		Expression rightSimpl = right.simplify();
		if(rightSimpl.isZero()) {
			return leftSimpl;
		}
		else if(rightSimpl.isEquivTo(leftSimpl)) {
			return new Constant(0);
		}
		else {
			return new Sub(leftSimpl, rightSimpl);
		}
	}
	
	@Override
	protected boolean isEquivTo(Expression other) {
		if(other instanceof Sub) {
			Sub that = (Sub)other;
			return (this.left.isEquivTo(that.left) && 
					this.right.isEquivTo(that.right));
		}
		return false;
	}

	@Override
	public double evaluate() throws ArithmeticException {
		return this.left.evaluate() - this.right.evaluate();
	}
}
