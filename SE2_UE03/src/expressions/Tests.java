package expressions;

import static org.junit.Assert.*;

import org.junit.Test;

public class Tests {

	@Test
	public void testToInfixString() {
		Expression exp = new Mult(new Variable("x"), new Add(new Constant(8), new Variable("y")));
		assertEquals("(x * (8.0 + y))", exp.toInfixString());
	}
	
	@Test
	public void testSimplify() {
		Expression exp = new Mult(new Add(new Variable("x"), new Constant(0)), new Add(new Constant(8), new Variable("y")));
		assertEquals("(x * (8.0 + y))", exp.simplify().toInfixString());
	}

}
