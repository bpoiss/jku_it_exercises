package expressions;

public class Variable extends Expression {
	protected String name;
	protected double value;
	
	public Variable(String name) {
		this.name = name;
	}

	@Override
	protected String toInfixString() {
		return name;
	}

	@Override
	protected Expression simplify() {
		return this;
	}

	@Override
	protected boolean isEquivTo(Expression expression) {
		if(expression instanceof Variable) {
			return this.name == ((Variable)expression).name ? true : false;
		}
		return false;
	}

	@Override
	protected double evaluate() {
		return this.value;
	}
}
