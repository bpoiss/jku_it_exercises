package inout;

/**
 * An implementation of this interface can be passed to Window.open(...),
 * and the redraw method will be called ~50 times per second.
 */
public interface RedrawHandler {
	
	/**
	 * This method is called whenever the window is repainted, about 50
	 * times per second. The supplied window can be used for drawing.
	 */
	void redraw(Window window, int width, int height);

}
