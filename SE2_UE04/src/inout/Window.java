package inout;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;


/**
 * This class allows for simple output of graphical objects on a window. 
 * It provides several methods to draw points, lines, circles and text. 
 */
public class Window {

	/** Constant for standard width */
	private static final int DEFAULT_WIDTH = 400;

	/** Constant for height */
	private static final int DEFAULT_HEIGHT = 400;
	
	/** X position of the next window that is openend */
	private static int xPosition = 0;
	
    protected Graphics g;
    
    private final RedrawHandler handler;
    
    /**
     * Opens a new window and periodically calls {@link RedrawHandler#redraw(Window)} until the window is closed.
     * @param handler a RedrawHandler instance that is used to periodically update the window contents.
     */
    public static void open(final RedrawHandler handler) {
        new Thread() {
            public void run() {
                new Window(handler);
            }
        }.start();
    }
    
    private Window(RedrawHandler handler) {
        this.handler = handler;
        if (GraphicsEnvironment.isHeadless()) {
            System.out.println("Graphical output will not work in a headless environment.");
            System.out.println("Please use -Djava.awt.headless=false VM parameter.");
        }
        AnimationFrame contents = new AnimationFrame();
        contents.setBounds(xPosition, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        xPosition += 100;
        contents.setVisible(true);
        contents.addWindowListener(new WindowAdapter() {
            
            public void windowClosing(WindowEvent evt) {
                Frame frame = (Frame)evt.getSource();
                frame.setVisible(false);
                frame.dispose();
            }
        });
        
        while (contents.isVisible()) {
            contents.repaint();
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
            }
        }       
    }

    public static void main(String[] args) {
        Window.open(new RedrawHandler() {
            int i = 0;

            @Override
            public void redraw(Window window, int width, int height) {
                window.drawTextCentered("asdfasdfasdf " + i, (int) (200 + Math.cos(i * 0.02) * 50), (int) (200 + Math.sin(i * 0.02) * 150),
                        Color.RED);
                i++;
            }
        });
        Window.open(new RedrawHandler() {
            int i = 0;

            @Override
            public void redraw(Window window, int width, int height) {
                window.drawTextCentered("asdfasdfasdf " + i, (int) (100 + Math.cos(i * 0.02) * 50), (int) (200 + Math.sin(i * 0.02) * 150),
                        Color.RED);
                i++;
            }
        });
        Window.open(new RedrawHandler() {
            int i = 0;

            @Override
            public void redraw(Window window, int width, int height) {
                window.drawTextCentered("asdfasdfasdf " + i, (int) (200 + Math.cos(i * 0.042) * 50), (int) (200 + Math.sin(i * 0.02) * 150),
                        Color.RED);
                i++;
            }
        });
    }

    /**
     * Static inner class for the content pane of the frame. Overrides the paint method and draws the image object.
     */
    @SuppressWarnings("serial")
    private class AnimationFrame extends Frame {
        private Image image = null;
        private int width = 0;
        private int height = 0;

        @Override
        public void update(Graphics g) {
            paint(g);
        }
        
        @Override
        public void paint(Graphics g) {
            int contentWidth = g.getClipBounds().width;
            int contentHeight = g.getClipBounds().height;
            if (image == null || width != contentWidth || height != contentHeight) {
                width = contentWidth;
                height = contentHeight;
                image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            }
            Window.this.g = image.getGraphics();
            if (Window.this.g != null) {
                Window.this.g.fillRect(0, 0, contentWidth, contentHeight);
                handler.redraw(Window.this, contentWidth, contentHeight);
                Window.this.g.dispose();
                g.drawImage(image, 0, 0, null);
            }
        }
    }

    /**
     * Computes the ascent of the current font, ascent is the distance between the baseline and the top border of the text.
     */
     private int getTextAscent() {
         FontMetrics fm = g.getFontMetrics();
         return fm.getAscent();
     }
     
    
    /** 
     * Computes the width of a string.
     * 
     * @param text the string 
     * @return the width of the string in pixel
     */
    public int getTextWidth(String text) {
        FontMetrics fm = g.getFontMetrics();
        return (int) Math.round(fm.getStringBounds(text, g).getWidth());
    }

   /** 
    * Computes the height of text. 
    * 
    * @return the height of text in pixel
    */
    public int getTextHeight() {
        FontMetrics fm = g.getFontMetrics();
        return fm.getHeight();
    }
    
// Methods for drawing 

    /**
     * Draws a point on position x and y
     * 
     * @param x the x-coordinate of the point
     * @param y the y-coordinate of the point
     */
    public void drawPoint (int x, int y) {
        g.setColor(Color.black);
        g.fillRect(x-1, y-1, 3, 3);
    }

    /** 
     * Draws a line from position (x1, y1) to position (x2, y2). 
     * 
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     */
    public void drawLine (int x1, int y1, int x2, int y2) {
        g.setColor(Color.black);
        g.drawLine(x1, y1, x2, y2);
    }

    /** 
     * Draws a rectangle on position (x, y) with width w and height h. 
     * 
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     * @param w the width of the rectangle 
     * @param h the height of the rectangle 
     */
    public void drawRectangle (int x, int y, int w, int h) {
        g.setColor(Color.black);
        g.drawRect(x, y, w, h);
    }

    /** 
     * Draws a circle at center (x, y) and radius r. 
     * 
     * @param x the x-coordinate of the center
     * @param y the y-coordinate of the center
     * @param r the radius of the circle 
     */
    public void drawCircle (int x, int y, int r) {
        g.setColor(Color.black);
        g.drawOval(x-r, y-r, 2*r, 2*r);
    }

    /** 
     * Draws the text left aligned at given position 
     * 
     * @param text the text to draw
     * @param x the x-coordinate
     * @param y the y-coordinate 
     */
    public void drawText(String text, int x, int y) {
    	g.setColor(Color.black);
    	g.drawString(text, x, y + getTextAscent());
    }

    /** 
     * Draws the text centered at given position
     * 
     * @param text the text to draw
     * @param x the x-coordinate 
     * @param y the y-coordinate 
     */
    public void drawTextCentered(String text, int x, int y) {
    	g.setColor(Color.black);
    	g.drawString(text, x - getTextWidth(text)/2, y + getTextAscent());
    }

    /**
     * Draws a point on position x and y with given color 
     * 
     * @param x the x-coordinate of the point
     * @param y the y-coordinate of the point
     */
    public void drawPoint (int x, int y, Color color) {
        g.setColor(color);
        g.fillRect(x-1, y-1, 3, 3);
    }

    /** 
     * Draws a line from position (x1, y1) to position (x2, y2) with given color. 
     * 
     * @param x1 the x-coordinate of the first point
     * @param y1 the y-coordinate of the first point
     * @param x2 the x-coordinate of the second point
     * @param y2 the y-coordinate of the second point
     */
    public void drawLine (int x1, int y1, int x2, int y2, Color color) {
        g.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue()));
        g.drawLine(x1, y1, x2, y2);
    }

    /** 
     * Draws a rectangle on position (x, y) with width w and height h with given color. 
     * 
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     * @param w the width of the rectangle 
     * @param h the height of the rectangle 
     */
    public void drawRectangle (int x, int y, int w, int h, Color color) {
        g.setColor(color);
        g.drawRect(x, y, w, h);
    }

    /** 
     * Draws a circle at center (x, y) and radius r with given color. 
     * 
     * @param x the x-coordinate of the center
     * @param y the y-coordinate of the center
     * @param r the radius of the circle 
     */
    public void drawCircle (int x, int y, int r, Color color) {
        g.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue()));
        g.drawOval(x-r, y-r, 2*r, 2*r);
    }

    /** 
     * Draws the text left aligned at given position with given color 
     * 
     * @param text the text to draw
     * @param x the x-coordinate 
     * @param y the y-coordinate 
     */
    public void drawText(String text, int x, int y, Color color) {
    	g.setColor(color);
    	g.drawString(text, x, y + getTextAscent());
    }

    /** 
     * Draws the text centered at given position
     * 
     * @param text the text to draw
     * @param x the x-coordinate 
     * @param y the y-coordinate 
     */
    public void drawTextCentered(String text, int x, int y, Color color) {
    	g.setColor(color);
    	g.drawString(text, x - getTextWidth(text)/2, y + getTextAscent());
    }

    /** 
     * Fills a rectangle on position (x, y) with width w and height h with given color. 
     * 
     * @param x the x-coordinate of the position
     * @param y the y-coordinate of the position
     * @param w the width of the rectangle 
     * @param h the height of the rectangle 
     */
    public void fillRectangle (int x, int y, int w, int h, Color color) {
        g.setColor(color);
        g.fillRect(x, y, w, h);
    }

    /** 
     * Fills a circle at center (x, y) and radius r with given color. 
     * 
     * @param x the x-coordinate of the center
     * @param y the y-coordinate of the center
     * @param r the radius of the circle 
     */
    public void fillCircle (int x, int y, int r, Color color) {
        g.setColor(color);
        g.fillOval(x-r, y-r, 2*r, 2*r);
    }

}
