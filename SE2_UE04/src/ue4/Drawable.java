package ue4;

import inout.Window;

public interface Drawable {
	/**
	 * Draws the Drawing to a Window
	 * @param window The window where the drawing is drawed
	 */
	void draw(Window window);
}
