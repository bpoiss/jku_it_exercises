package ue4;

import java.awt.Color;

import inout.Window;

public class MovingCircle extends MovingDrawable {
	private int r, n = 0;
	private Color color = Color.black;
	
	public MovingCircle(int r, Color color) {
		this.color = color;
		this.r = r;
	}
	
	@Override
	void drawOn(Window window, int x, int y) {
		this.n++;
		window.fillCircle(getX(n), getY(n), r, this.color);
	}

	@Override
	int getX(int n) {
		return (int) (100 * Math.sin(0.05 * n + Math.PI/4 )) + 250;		 
	}

	@Override
	int getY(int n) {
		return (int) (100 * Math.cos(0.1 * n)) + 250;
	}
	
}
