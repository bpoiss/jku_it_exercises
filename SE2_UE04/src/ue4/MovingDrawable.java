package ue4;

import inout.Window;

public abstract class MovingDrawable implements Drawable{
	private int n = 0;
	
	/**
	 * Draws the Drawing to window, on position specified by x and y coordinates
	 * @param window The window to draw the drawing
	 * @param x	X-Coordinate
	 * @param y Y-Coordinage
	 */
	abstract void drawOn(Window window, int x, int y);
	
	/**
	 * Calculates the X-Coordinate by param n
	 * @param n Counter how often the draw function was called
	 * @return Calculated X-Coordinate
	 */
	abstract int getX(int n);
	
	/**
	 * Calculates the Y-Coordinate by param n
	 * @param n Counter how often the draw function was called
	 * @return Calculated Y-Coordinate
	 */
	abstract int getY(int n);
	
	@Override
	public void draw(Window window) {
		n++;
		drawOn(window, getX(n), getY(n));
	}
}
