package ue4;

import java.awt.Color;

import inout.Window;

public class MovingRect extends MovingDrawable {
	private int w, h, n = 0;
	private Color color = Color.black;
	
	public MovingRect(int w, int h, Color color) {
		this.color = color;
		this.w = w;
		this.h = h;
	}
	
	@Override
	void drawOn(Window window, int x, int y) {
		this.n++;
		window.fillRectangle(getX(n), getY(n), w, h, this.color);
	}

	@Override
	int getX(int n) {
		return (int) (100 * Math.sin(0.15 * n + Math.PI/4 )) + 150;		 
	}

	@Override
	int getY(int n) {
		return (int) (100 * Math.cos(0.1 * n)) + 150;
	}
	
}
