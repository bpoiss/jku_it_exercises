package ue4;

import java.util.ArrayList;
import inout.*;

public class SceneHandler implements RedrawHandler {
	
	private ArrayList<Drawable> drawables = new ArrayList<>();
	
	/**
	 * Adds a Drawable to the scenehandler
	 * @param d Drawable to be added
	 */
	void addDrawable(Drawable d) {
		drawables.add(d);
	}
	
	@Override
	public void redraw(Window window, int width, int height) {
		for(Drawable drawable : drawables) {
			drawable.draw(window);
		}
	}

}
