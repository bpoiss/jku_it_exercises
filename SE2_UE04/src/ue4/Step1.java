package ue4;

import java.awt.Color;

import inout.*;

public class Step1 {	
	public static void main(String[] args) {		
		Window.open(new RedrawHandler() {
			
			@Override
			public void redraw(Window window, int width, int height) {
				window.drawCircle(width/2, height/2, 10);
			}
		});
		
		Window.open(new RedrawHandler() {
			
			@Override
			public void redraw(Window window, int width, int height) {
				window.drawTextCentered("Hello world", width/2, height/2, Color.red);
			}
		});
		
		Window.open(new RedrawHandler() {
			
			@Override
			public void redraw(Window window, int width, int height) {
				window.drawRectangle(width/2, height/2, 10, 10);
			}
		});
	}
}
