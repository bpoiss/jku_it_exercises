package ue4;

import java.awt.Color;

import inout.*;

public class Step2 {
	public static void main(String[] args) {
		SceneHandler sceneHandler = new SceneHandler();
		Window.open(sceneHandler);

		sceneHandler.addDrawable(new Drawable() {
			
			@Override
			public void draw(Window window) {
				window.fillCircle(111, 111, 40, Color.green);
			}
		});
		
		sceneHandler.addDrawable(new Drawable() {
			
			@Override
			public void draw(Window window) {
				window.drawRectangle(150, 150, 11, 11);
			}
		});
		
		sceneHandler.addDrawable(new Drawable() {
			
			@Override
			public void draw(Window window) {
				window.drawText("Hello world", 222, 222, Color.red);
			}
		});
	}
}
