package ue4;

import java.util.Date;

import inout.Window;

public class Step3 {
	public static void main(String[] args) {
		SceneHandler sceneHandler = new SceneHandler();
		Window.open(sceneHandler);
		
		class DateTimeTextDrawable extends TextDrawable {
			public DateTimeTextDrawable(int x, int y) {
				super(x, y);
			}

			@Override
			public String getText() {
				return new Date().toString();
			}
		}
		
		class LoopCounterDrawable extends TextDrawable {
			private int loopCounter = 0;
			
			public LoopCounterDrawable(int x, int y) {
				super(x, y);
			}

			@Override
			public String getText() {
				return String.valueOf(loopCounter++);
			}
		}

		sceneHandler.addDrawable(new DateTimeTextDrawable(150, 150));
		sceneHandler.addDrawable(new LoopCounterDrawable(150, 50));
	}	
}
