package ue4;

import inout.Window;

import java.awt.Color;

public class Step4 {
	public static void main(String[] args) {
		SceneHandler sceneHandler = new SceneHandler();
		Window.open(sceneHandler);
		MovingCircle movingCircle = new MovingCircle(10, Color.red);
		MovingRect movingRect = new MovingRect(10, 10, Color.blue);
		
		sceneHandler.addDrawable(movingCircle);
		sceneHandler.addDrawable(movingRect);
	}
}
