package ue4;

import inout.Window;

public abstract class TextDrawable implements Drawable {
	protected int x, y;
	
	public TextDrawable(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void draw(Window window) {
		window.drawTextCentered(getText(), x, y);		
	}
	
	/**
	 * Returns the text to be drawn
	 * @return Text to be drawn
	 */
	public abstract String getText();
	
}
