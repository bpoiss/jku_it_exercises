package arithm;

public interface Arithm<T> {
	public T plus(T x, T y);
	public T sub(T x, T y);
	public T mult(T x, T y);
	public T div(T x, T y);
	public T minus(T x);
}