package arithm;

import blocks.*;

public abstract class ArithmBinaryBlock<T> extends Block {

	public final Arithm<T> op; 
	public final Inport<T> in1; 
	public final Inport<T> in2;
	public final Outport<T> out; 

	public ArithmBinaryBlock(String text,Arithm<T> op) {
		super(text);
		this.op = op;
		in1 = new Inport<T>(this);
		in2 = new Inport<T>(this);
		out = new Outport<T>(this);
	}

}
