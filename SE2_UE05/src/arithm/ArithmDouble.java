package arithm;

public class ArithmDouble implements Arithm<Double> {

	@Override
	public Double plus(Double x, Double y) {
		return x + y;
	}

	@Override
	public Double sub(Double x, Double y) {
		return x - y;
	}

	@Override
	public Double mult(Double x, Double y) {
		return x * y;
	}

	@Override
	public Double div(Double x, Double y) {
		return x / y;
	}

	@Override
	public Double minus(Double x) {
		return -x;
	}
}
