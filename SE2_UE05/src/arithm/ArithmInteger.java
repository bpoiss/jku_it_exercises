package arithm;

public class ArithmInteger implements Arithm<Integer> {

	@Override
	public Integer plus(Integer x, Integer y) {
		return x + y;
	}

	@Override
	public Integer sub(Integer x, Integer y) {
		return x - y;
	}

	@Override
	public Integer mult(Integer x, Integer y) {
		return x * y;
	}

	@Override
	public Integer div(Integer x, Integer y) {
		return x / y;
	}

	@Override
	public Integer minus(Integer x) {
		return -x;
	}
}
