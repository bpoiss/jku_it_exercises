package arithm;

public class ArithmMod5 implements Comparable<ArithmMod5>, Arithm<Integer> {

	@Override
	public Integer plus(Integer x, Integer y) {
		return (x + y) % 5;
	}

	@Override
	public Integer sub(Integer x, Integer y) {
		return (x - y) % 5;
	}

	@Override
	public Integer mult(Integer x, Integer y) {
		return (x * y) % 5;
	}

	@Override
	public Integer div(Integer x, Integer y) {
		return (x / y) % 5;
	}

	@Override
	public Integer minus(Integer x) {
		return (x % 5) * -1;
	}

	@Override
	public int compareTo(ArithmMod5 o) {
		return o.compareTo(this);
	}

}
