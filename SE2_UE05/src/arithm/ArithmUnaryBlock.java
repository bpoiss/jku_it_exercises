package arithm;

import blocks.*;

public abstract class ArithmUnaryBlock<T> extends Block {

	public final Arithm<T> op; 
	public final Inport<T> in1; 
	public final Outport<T> out; 

	public ArithmUnaryBlock(String text,Arithm<T> op) {
		super(text);
		this.op = op;
		in1 = new Inport<T>(this);
		out = new Outport<T>(this);
	}

}
