package arithm;

public class Div<T extends Arithm> extends ArithmBinaryBlock<T> {

	public Div(String text, Arithm<T> op) {
		super(text, op);
	}

	@Override
	public void eval() {
		out.set(op.div(in1.get(), in2.get()));
	}

}
