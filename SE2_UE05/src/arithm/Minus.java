package arithm;

public class Minus<T extends Arithm> extends ArithmUnaryBlock<T> {

	public Minus(String text, Arithm<T> op) {
		super(text, op);
	}

	@Override
	public void eval() {
		out.set(op.minus(in1.get()));
	}

}
