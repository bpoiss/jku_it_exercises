package arithm;

public class Mult<T> extends ArithmBinaryBlock<T> {

	public Mult(String text, Arithm<T> op) {
		super(text, op);
	}

	@Override
	public void eval() {
		out.set(op.mult(in1.get(), in2.get()));
	}

}
