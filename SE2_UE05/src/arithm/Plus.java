package arithm;

public class Plus<T> extends ArithmBinaryBlock<T> {

	public Plus(String text, Arithm<T> op) {
		super(text, op);
	}

	@Override
	public void eval() {
		out.set(op.plus(in1.get(), in2.get()));
	}

}
