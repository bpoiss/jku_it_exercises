package arithm;

public class Sub<T extends Arithm> extends ArithmBinaryBlock<T> {

	public Sub(String text, Arithm<T> op) {
		super(text, op);
	}

	@Override
	public void eval() {
		out.set(op.sub(in1.get(), in2.get()));
	}

}
