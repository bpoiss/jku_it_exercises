package blocks;

public abstract class BinaryBlock<T> extends Block {
	
	public final Inport<T> in1; 
	public final Inport<T> in2;
	public final Outport<T> out; 
	
	public BinaryBlock(String text) {
		super(text);
		in1 = new Inport<T>(this);
		in2 = new Inport<T>(this);
		out = new Outport<T>(this);
	}

}
