package blocks;

import static blocks.Constants.HEIGHT;
import static blocks.Constants.PORT_WIDTH;
import static blocks.Constants.TEXT_HEIGHT;
import static blocks.Constants.WIDTH;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * Base class for function blocks. Defines name, position on screen, and abstract
 * method eval.
 */
public abstract class Block {

	/**
	 * Connects an output port to an input port
	 * 
	 * @param outp the output port
	 * @param inp the input port
	 */
	public static <T> void connect(Outport<T> outp, Inport<? super T> inp) {
		outp.connectTo(inp);
	}

	/** Name of the function block */
	public final String name;

	/**
	 * Constructor initializing name and position
	 * 
	 * @param name the name of the function block
	 */
	public Block(String name) {
		this.name = name;
		addBlock(this);
	}

	/**
	 * Evaluates this function block. Subclasses should implement this method to
	 * implement the function of the function block.
	 */
	abstract public void eval();

	/**
	 * Draws the function block with name and input and output ports on the output
	 * frame.
	 * 
	 * @param g the graphics context
	 */
	protected void draw(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(x + PORT_WIDTH / 2, y, WIDTH - PORT_WIDTH, HEIGHT);
		g.setColor(Color.BLACK);
		g.drawRect(x + PORT_WIDTH / 2, y, WIDTH - PORT_WIDTH, HEIGHT);
		g.drawString(name, x + PORT_WIDTH + 4, y + TEXT_HEIGHT + 2);
		for (Object obj : inports) {
			Inport<?> inp = (Inport<?>) obj;
			inp.draw(g);
		}
		for (Object obj : outports) {
			Outport<?> outp = (Outport<?>) obj;
			outp.draw(g);
		}
	}

	// internal ---------------------------------------------------------------

	int x, y;
	final List<Inport<?>> inports = new ArrayList<>();
	final List<Outport<?>> outports = new ArrayList<>();

	private static List<Block> blocks = Collections.synchronizedList(new ArrayList<Block>());
	private static boolean layoutNeeded = false;
	protected static JFrame frame;

	static {
		frame = new JFrame("Blocks");
		frame.setSize(900, 700);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setContentPane(new BlocksPanel());
		frame.getContentPane().add(new JLabel("TEST"));
		frame.setVisible(true);
	}

	void addInport(Inport<?> inp) {
		inports.add(inp);
		layoutNeeded = true;
	}

	void addOutput(Outport<?> outp) {
		outports.add(outp);
		layoutNeeded = true;
	}
	
	protected void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	private static void addBlock(Block block) {
		blocks.add(block);
		layoutNeeded = true;
	}
	
	private boolean isSource() {
		for (Inport<?> in : inports) {
			if (in.connected != null) {
				return false;
			}
		}
		return true;
	}
	
	private boolean allInputsIn(List<Block> blocks) {
		for (Inport<?> in : inports) {
			if (in.connected != null && !blocks.contains(in.connected.block)) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isSink() {
		for (Outport<?> out : outports) {
			if (!out.connected.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	private static void createLayout() {
		int x = 20;
		int maxHeight = 100;
		List<Block> lastList = new ArrayList<Block>();
		while (lastList.size() < blocks.size()) {
			if (x > 10000) {
				System.out.println("cannot create layout - circular dependencies?");
				System.exit(-1);
			}
			List<Block> nextList = new ArrayList<Block>();
			if (lastList.isEmpty()) {
				for (Block block : blocks) {
					if (block.isSource()) {
						nextList.add(block);
					}
				}
			} else {
				for (Block last : lastList) {
					for (Outport<?> out : last.outports) {
						for (Inport<?> in : out.connected) {
							Block block = in.block;
							if (!lastList.contains(block) && !nextList.contains(block)) {
								if (block.allInputsIn(lastList)) {
									nextList.add(block);
								}
							}
						}
					}
				}
			}
			if (lastList.size() + nextList.size() < blocks.size()) {
				for (Iterator<Block> iter = nextList.iterator(); iter.hasNext();) {
					if (iter.next().isSink()) {
						iter.remove();
					}
				}
			}
			int y = 30;
			for (Block block : nextList) {
				block.setPosition(x, y);
				y += HEIGHT + 10;
			}
			maxHeight = Math.max(maxHeight, y);
			nextList.addAll(lastList);
			lastList = nextList;
			x += WIDTH + 50;
		}
		frame.setSize(x, maxHeight + 30);
	}

	@SuppressWarnings("serial")
	static class BlocksPanel extends JPanel {

		BlocksPanel() {
			setLayout(null);
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			try {
				if (layoutNeeded) {
					createLayout();
					layoutNeeded = false;
				}
				for (Block b : blocks) {
					b.draw(g);
				}
			} catch (Exception e) {
				repaint();
			}
		}

	}
}
