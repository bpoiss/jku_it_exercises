package blocks;

public class BooleanAnd extends BinaryBlock<Boolean> {

	public BooleanAnd(String text) {
		super(text);
	}

	@Override
	public void eval() {
		out.set(in1.get() && in2.get() ? true : false); 
	}

}
