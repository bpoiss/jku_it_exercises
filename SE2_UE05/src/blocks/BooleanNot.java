package blocks;

public class BooleanNot extends UnaryBlock<Boolean> {

	public BooleanNot(String text) {
		super(text);
	}

	@Override
	public void eval() {
		out.set(!in.get()); 
	}

}
