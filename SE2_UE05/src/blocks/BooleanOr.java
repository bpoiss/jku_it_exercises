package blocks;

public class BooleanOr extends BinaryBlock<Boolean> {

	public BooleanOr(String text) {
		super(text);
	}

	@Override
	public void eval() {
		out.set(in1.get() || in2.get() ? true : false); 
	}

}
