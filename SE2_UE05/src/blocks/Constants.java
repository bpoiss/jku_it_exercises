package blocks;

/**
 * Class with static constants for configuring visual output
 */
public class Constants {

	/** the width of a function block */
	public static final int WIDTH = 140; 
	
	/** the height of a function block */
	public static final int HEIGHT = 60; 
	
	/** the width of a port */
	public static final int PORT_WIDTH = 40; 
	
	/** the height of a port */
	public static final int PORT_HEIGHT = 13;
	
	/** the text height */
	public static final int TEXT_HEIGHT = 10; 
	
}
