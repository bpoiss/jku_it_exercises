package blocks;

public class IdFn<T> extends Block {
	
	public final Inport<T> in; 
	public final Outport<T> out; 

	public IdFn() {
		super("id");
		in = new Inport<T>(this);
		out = new Outport<T>(this);
	}

	@Override
	public void eval() {
		out.set(in.get()); 
	}

}
