package blocks;

import static blocks.Constants.HEIGHT;
import static blocks.Constants.PORT_HEIGHT;
import static blocks.Constants.PORT_WIDTH;
import static blocks.Constants.TEXT_HEIGHT;

import java.awt.Color;
import java.awt.Graphics;

/** 
 * Class implementing input ports. Is generic for the input values. 
 * Must be initialized with the function block this port belongs to. 
 * Provides method for retrieving the value of this input port. 
 * 
 * @param <T> type of input values
 */
public final class Inport<T> {
	
	/**
	 * Constructor setting the function block this port belongs to
	 * @param block function block this port belongs to
	 */
	public Inport(Block block) {
		super();
		this.block = block;
		block.addInport(this); 
	}

	/**
	 * Gets the value of this input port. 
	 * @return the value of this input port
	 */
	public T get() {
		return value; 
	}

	// internal ---------------------------------------------------------------
	
	final Block block; 
	Outport<? extends T> connected; 
	private T value; 
	
	void set(T value) {
		if (this.value != null && this.value.equals(value)) 
			return; 
		this.value = value; 
		try {
			block.eval(); 
		} catch (Exception e) {
		}
		Block.frame.getContentPane().repaint();
	}
	
	int getX() {
		return block.x;
	}

	int getY() {
		int idx = getIdx(); 
		int dy = HEIGHT / (block.inports.size() + 1);  
		return block.y + dy / 2 + idx * dy ; 
	}

	int getIdx() {
		return block.inports.indexOf(this);
	}

	void draw(Graphics g) {
		int px = getX(); 
		int py = getY();
		g.setColor(Color.WHITE);
		g.fillRect(px, py, PORT_WIDTH, PORT_HEIGHT); 
		g.setColor(Color.BLACK); 
		g.drawRect(px, py, PORT_WIDTH, PORT_HEIGHT); 
		if (get() != null) {
			g.drawString(get().toString(), px  + 2, py + TEXT_HEIGHT + 2); 
		} else {
			g.drawString("null", px  + 2, py + TEXT_HEIGHT + 2); 
			
		}

	}

}
