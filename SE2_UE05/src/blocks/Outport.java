package blocks;

import static blocks.Constants.HEIGHT;
import static blocks.Constants.PORT_HEIGHT;
import static blocks.Constants.PORT_WIDTH;
import static blocks.Constants.TEXT_HEIGHT;
import static blocks.Constants.WIDTH;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

/** 
 * Class implementing output ports. Is generic for the output values. 
 * Must be initialized with the function block this port belongs to. 
 * Provides method for setting the value of this input port. 
 * 
 * @param <T> type of output values
 */
public final class Outport<T> {
	
	/**
	 * Constructor setting the function block this port belongs to
	 * @param block function block this port belongs to
	 */
	public Outport(Block block) {
		super();
		this.block = block;
		connected = new ArrayList<>(); 
		block.addOutput(this); 
	}
	
	/**
	 * Sets the value of this output port. 
	 * @param value the value to set 
	 */
	public void set(T value) {
		if (this.value == null || ! this.value.equals(value)) {
			this.value = value; 
			propagate();  
		}
	}

	// internal ---------------------------------------------------------------

	final Block block; 
	final List<Inport<? super T>> connected; 
	private T value; 
	
	void connectTo(Inport<? super T> input) {
		connected.add(input);
		input.connected = this;  
		input.set(value);
	}

	T get() {
		return value; 
	}

	int getX() {
		return block.x + WIDTH - PORT_WIDTH;
	}
	
	int getY() {
		int idx = getIdx(); 
		int dy = HEIGHT / (block.outports.size() + 1);  
		return block.y + dy / 2 + idx * dy ; 
	}

	int getIdx() {
		return block.outports.indexOf(this);
	}

	void draw(Graphics g) {
		int py = getY(); 
		int px = getX(); 
		g.setColor(Color.WHITE);
		g.fillRect(px, py, PORT_WIDTH, PORT_HEIGHT); 
		g.setColor(Color.BLACK);
		g.drawRect(px, py, PORT_WIDTH, PORT_HEIGHT); 
		if (get() != null) {
			g.drawString(get().toString(), px + 2, py + TEXT_HEIGHT + 2); 
		} else {
			g.drawString("null", px + 2, py + TEXT_HEIGHT + 2); 			
		}
		for (Inport<?> inp : connected) {
			g.drawLine(getX() + PORT_WIDTH, getY() + PORT_HEIGHT/2, 
					inp.getX(), inp.getY() + PORT_HEIGHT/2);
		}
	}

	private void propagate() {
		for (Inport<? super T> inp: connected) {
			inp.set(value); 
		}
		Block.frame.getContentPane().repaint();
	}
	
}
