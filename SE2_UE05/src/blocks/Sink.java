package blocks;

import static blocks.Constants.HEIGHT;
import static blocks.Constants.PORT_WIDTH;

import java.awt.Graphics;

/**
 * Sink block defining one input port. 
 * Is intended to provide a sink of a computation and displays the result value
 * Is not generic but can receive any value at its input port.
 */
public class Sink extends Block {
	
	/** Input port with value types Object*/
	public Inport<Object> in;

	/** 
	 * Constructor initializing name and position
	 * @param name the name of the function block
	 */
	public Sink(String name) {
		super(name);
		in = new Inport<Object>(this);
	}


	/**
	 * Sets the input value as result value
	 */
	@Override
	public void eval() {
		result = in.get(); 
	}

	/**
	 * Returns the result value received at input port. 
	 * @return the result received at input port. 
	 */
	public Object getResult() {
		return result; 
	}
	
	/* (non-Javadoc)
	 * @see blocks.Block#draw(java.awt.Graphics)
	 */
	@Override
	protected void draw(Graphics g) {
		super.draw(g);
		if (result != null) 
			g.drawString(result.toString(), x + PORT_WIDTH + 5, y + HEIGHT / 2); 
	} 
	
	// internal ---------------------------------------------------------------

	private Object result;
	
}
