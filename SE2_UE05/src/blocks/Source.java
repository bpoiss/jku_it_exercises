package blocks;

import static blocks.Constants.PORT_WIDTH;
import static blocks.Constants.WIDTH;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

/**
 * Block allowing input of values by a user. Uses a JTextField for input. 
 * The block is generic and abstract and serves as a base class for implementing 
 * input of different data types. 
 * Subclasses have to implement parse and initial to generate a should serve  
 */
public abstract class Source<T> extends Block {
	
	public final Outport<T> out;
	
	public Source(String name) {
		super(name);
		out = new Outport<T>(this);
		out.set(initial()); 
		inpField = new JTextField(initial().toString()); 
		inpField.setBounds(x + PORT_WIDTH / 2 + 4, y + 20, WIDTH - PORT_WIDTH - PORT_WIDTH / 2 - 8, 20);
		Block.frame.getContentPane().add(inpField);
		inpField.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				eval();
			}
			
		});
	}

	@Override
	public final void eval() {
		try {
			out.set(parse(inpField.getText()));
		} catch (Exception e) {
			inpField.setText(out.get().toString());
		}
	}

	protected abstract T initial(); 
	
	protected abstract T parse(String text);

	// internal ---------------------------------------------------------------
	
	private final JTextField inpField; 
	
	@Override
	protected void setPosition(int x, int y) {
		super.setPosition(x, y);
		inpField.setBounds(x + PORT_WIDTH / 2 + 4, y + 20, WIDTH - PORT_WIDTH - PORT_WIDTH / 2 - 8, 20);
	}
}
