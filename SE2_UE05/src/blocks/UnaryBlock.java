package blocks;

public abstract class UnaryBlock<T> extends Block {
	
	public final Inport<T> in; 
	public final Outport<T> out; 

	public UnaryBlock(String text) {
		super(text);
		in = new Inport<T>(this);
		out = new Outport<T>(this);
	}

}
