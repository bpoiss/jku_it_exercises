package comparable;

import blocks.*;

public abstract class ComparableBlock<T extends Comparable<T>> extends Block {
	public final Inport<T> in1, in2;
	public final Outport<T> out;

	public abstract T compare(T in1, T in2);
	
	public ComparableBlock(String text) {
		super(text);
		in1 = new Inport<T>(this);
		in2 = new Inport<T>(this);
		out = new Outport<T>(this);
	}

	@Override
	public void eval() {
		out.set(compare(in1.get(), in2.get()));
	}

}