package comparable;

import blocks.Block;

public class Greater<T extends Comparable<T>> extends ComparableBlock<T> {

	public Greater(String text) {
		super(text);
	}

	@Override
	public T compare(T in1, T in2) {
		return (T) Boolean.valueOf(in1.compareTo(in2) >= -1);
	} 

}
