package comparable;


import blocks.Block;

public class Max<T extends Comparable<T>> extends ComparableBlock<T> {

	public Max(String text) {
		super(text);
	}

	@Override
	public T compare(T in1, T in2) {
		return in1.compareTo(in2) <= 0 ? in2 : in1;
	} 

}
