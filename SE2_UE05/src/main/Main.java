package main;

import sources.BooleanSource;
import sources.DoubleSource;
import sources.IntegerSource;
import sources.StringSource;
import comparable.*;

import arithm.ArithmDouble;
import arithm.ArithmMod5;
import arithm.Plus;
import blocks.*;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//testSources();		
		//testBooleanAnd();
		//testBooleanOr();
		//testBooleanNot();		
		//testEqual();
		//testGreater();
		//testMax();
		//testArithm();
		testMod5();
	}

	private static void testBooleanAnd() {
		BooleanAnd booleanAnd = new BooleanAnd("Boolean AND");
		BooleanSource booleanSource1 = new BooleanSource("Bool 1");
		BooleanSource booleanSource2 = new BooleanSource("Bool 2");
		Sink sink = new Sink("sink");
		
		Block.connect(booleanSource1.out, booleanAnd.in1);
		Block.connect(booleanSource2.out, booleanAnd.in2);
		Block.connect(booleanAnd.out, sink.in);
	}

	private static void testBooleanOr() {
		BooleanOr booleanOr = new BooleanOr("Boolean OR");
		BooleanSource booleanSource1 = new BooleanSource("Bool 1");
		BooleanSource booleanSource2 = new BooleanSource("Bool 2");
		Sink sink = new Sink("sink");
		
		Block.connect(booleanSource1.out, booleanOr.in1);
		Block.connect(booleanSource2.out, booleanOr.in2);
		Block.connect(booleanOr.out, sink.in);
	}

	private static void testBooleanNot() {
		BooleanNot booleanNot = new BooleanNot("Boolean NOT");
		BooleanSource booleanSource1 = new BooleanSource("Bool 1");
		Sink sink = new Sink("sink");
		
		Block.connect(booleanSource1.out, booleanNot.in);
		Block.connect(booleanNot.out, sink.in);
	}
	
	private static void testSources() {
		IdFn<Integer> id1 = new IdFn<Integer>();
		IdFn<String> id2 = new IdFn<String>();
		IdFn<Boolean> id3 = new IdFn<Boolean>();
		IdFn<Double> id4 = new IdFn<Double>();
				
		IntegerSource integerSource = new IntegerSource("Integer");
		StringSource stringSource = new StringSource("String");
		BooleanSource booleanSource = new BooleanSource("Bool");
		DoubleSource doubleSource = new DoubleSource("Double");

		Sink sink1 = new Sink("sink");
		Sink sink2 = new Sink("sink");
		Sink sink3 = new Sink("sink");
		Sink sink4 = new Sink("sink");
		
		Block.connect(integerSource.out, id1.in);
		Block.connect(id1.out, sink1.in);
		
		Block.connect(stringSource.out, id2.in);
		Block.connect(id2.out, sink2.in);
		
		Block.connect(booleanSource.out, id3.in);
		Block.connect(id3.out, sink3.in);
		
		Block.connect(doubleSource.out, id4.in);
		Block.connect(id4.out, sink4.in);
	}

	private static void testEqual() {
		Equal<String> equal = new Equal<>("Equal");
		IntegerSource intSource1 = new IntegerSource("Eingabe 1");
		IntegerSource intSource2 = new IntegerSource("Eingabe 2");
		Sink sink = new Sink("sink");

		Block.connect(intSource1.out, equal.in1);
		Block.connect(intSource2.out, equal.in2);
		Block.connect(equal.out, sink.in);
	}
	
	private static void testGreater() {
		Greater<String> greater = new Greater<>("Greater");
		StringSource stringSource1 = new StringSource("Eingabe 1");
		StringSource stringSource2 = new StringSource("Eingabe 2");
		Sink sink = new Sink("sink");

		Block.connect(stringSource1.out, greater.in1);
		Block.connect(stringSource2.out, greater.in2);
		Block.connect(greater.out, sink.in);
	}
	
	private static void testMax() {
		Max<String> max = new Max<>("Max");
		IntegerSource intSource1 = new IntegerSource("Eingabe 1");
		IntegerSource intSource2 = new IntegerSource("Eingabe 2");
		Sink sink = new Sink("sink");

		Block.connect(intSource1.out, max.in1);
		Block.connect(intSource2.out, max.in2);
		Block.connect(max.out, sink.in);		
	}
	
	private static void testArithm() {
		Plus p = new Plus("Plus", new ArithmDouble());
		DoubleSource doubleSource1 = new DoubleSource("Eingabe 1");
		DoubleSource doubleSource2 = new DoubleSource("Eingabe 2");
		Sink sink = new Sink("sink");

		Block.connect(doubleSource1.out, p.in1);
		Block.connect(doubleSource2.out, p.in2);
		Block.connect(p.out, sink.in);
	}

	private static void testMod5() {
		Plus p = new Plus("Modolo 5", new ArithmMod5());
		IntegerSource integerSource1 = new IntegerSource("Eingabe 1");
		IntegerSource integerSource2 = new IntegerSource("Eingabe 2");
		Sink sink = new Sink("sink");

		Block.connect(integerSource1.out, p.in1);
		Block.connect(integerSource2.out, p.in2);
		Block.connect(p.out, sink.in);
	}
}
