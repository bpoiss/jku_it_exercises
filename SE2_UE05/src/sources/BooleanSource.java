package sources;

import blocks.Source;

public class BooleanSource extends Source {

	public BooleanSource(String name) {
		super(name);
	}

	@Override
	protected Boolean initial() {
		return false;
	}

	@Override
	protected Boolean parse(String input) {
		return Boolean.parseBoolean(input);
	}

}
