package sources;

import blocks.Source;

public class DoubleSource extends Source {

	public DoubleSource(String name) {
		super(name);
	}

	@Override
	protected Double initial() {
		return 0.0;
	}

	@Override
	protected Double parse(String input) {
		return Double.parseDouble(input);
	}

}
