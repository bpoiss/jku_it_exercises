package sources;

import blocks.Source;

public class IntegerSource extends Source {

	public IntegerSource(String name) {
		super(name);
	}

	@Override
	protected Integer initial() {
		return 0;
	}

	@Override
	protected Integer parse(String input) {
		return Integer.parseInt(input);
	}

}
