package sources;

import blocks.Source;

public class Mod5Source extends IntegerSource {

	public Mod5Source(String name) {
		super(name);
	}

	@Override
	protected Integer parse(String input) {
		return Integer.parseInt(input) % 5;
	}

}
