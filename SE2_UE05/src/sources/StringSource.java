package sources;

import blocks.Source;

public class StringSource extends Source {

	public StringSource(String name) {
		super(name);
	}

	@Override
	protected String initial() {
		return "";
	}

	@Override
	protected String parse(String input) {
		return input;
	}

}
