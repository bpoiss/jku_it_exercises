package relatives;

import java.util.*;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Relatives relatives = new Relatives(PersonData.DATA);

		System.out.println("Nachfahren von Pauletta:");
		for (Person p : relatives.getDescendants(relatives
				.getPerson("Pauletta"))) {
			System.out.println("   " + p.getName());
		}
		System.out.println();
		System.out.println("Nachfahren von Darius:");
		for (Person p : relatives.getDescendants(relatives.getPerson("Darius"))) {
			System.out.println("   " + p.getName());
		}
		System.out.println();
		System.out.println("Nachfahren von Joni:");
		for (Person p : relatives.getDescendants(relatives.getPerson("Joni"))) {
			System.out.println("   " + p.getName());
		}
		System.out.println();

		Collection<Person> dariusAndJoni = new ArrayList<>();
		dariusAndJoni.add(relatives.getPerson("Darius"));
		dariusAndJoni.add(relatives.getPerson("Joni"));

		System.out.println("Gemeinsame Nachfahren von Darius und Joni");
		for (Person p : relatives.getCommonDescendants(dariusAndJoni)) {
			System.out.println("   " + p.getName());
		}
		System.out.println();

		Collection<Person> adeliaAndLelah = new ArrayList<>();
		adeliaAndLelah.add(relatives.getPerson("Adelia"));
		adeliaAndLelah.add(relatives.getPerson("Lelah"));

		System.out.println("Gemeinsame Vorfahren von Adelia und Lelah");
		for (Person p : relatives.getCommonAncestors(adeliaAndLelah)) {
			System.out.println("   " + p.getName());
		}
		System.out.println();

		// -------------
		System.out.println("Anzahl Nachfahren 		Person");

		Map<Person, Integer> persons = relatives.getNumberOfDescendants();
		Comparator<Person> comp = relatives.getAncestorComparator();

		List<Person> list = new ArrayList<>(relatives.getPersons());
		Collections.sort(list, relatives.getDescendentsComparator());

		for (Person p : list) {
			System.out.println(relatives.getNumberOfDescendants(p) + "			" + p.getName());
		}
		System.out.println();
		// -----------------

		System.out.println("Folgende Personen haben genau 2 Nachfahren:");
		for (Person p : relatives.getPersons()) {
			if (relatives.getNumberOfAncestors(p) == 2) {
				System.out.println("   " + p.getName());
			}
		}
		System.out.println();

		int number = relatives.getNumberOfAncestors(relatives
				.getPerson("Allena"))
				+ relatives.getNumberOfDescendants(relatives
						.getPerson("Allena"));
		
		System.out.println("Die Familie von Allena hat " + number
				+ " Vorfahren und Nachfahren");
		
		System.out.println();
		System.out.println("Alle Frauen die keine Kinder haben:");
		
		List<Person> womenWithoutChildren = new ArrayList<>(relatives.getPersons());
		Collections.sort(womenWithoutChildren, relatives.getDescendentsComparator());
		
		for(Person p : womenWithoutChildren) {
			if(p.getSex().equals("F") && relatives.getChildren(p).size() == 0)
			System.out.println("   " + p.getName());
		}
	}

}
