package relatives;

public class Person {
	private String name, sex, fathersName, mothersName, partnersName;

	public Person(String name, String sex, String fathersName,
			String mothersName, String partnersName) {
		this.name = name;
		this.sex = sex;
		this.fathersName = fathersName;
		this.mothersName = mothersName;
		this.partnersName = partnersName;
	}

	public String getName() {
		return name;
	}

	public String getSex() {
		return sex;
	}

	public String getFathersName() {
		return fathersName;
	}

	public String getMothersName() {
		return mothersName;
	}

	public String getPartnersName() {
		return partnersName;
	}

}
