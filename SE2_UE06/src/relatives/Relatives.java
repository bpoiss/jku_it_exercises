package relatives;

import java.lang.reflect.Array;
import java.util.*;

public class Relatives {
	Collection<Person> persons = new ArrayList<>();
	
	public Relatives(String[] data) {
		String[] personData;

		for (String person : data) {
			personData = person.split(" ");
			persons.add(new Person(personData[0], personData[1],
					personData[2], personData[3], personData[4]));
		}
	}
	
	public Person getPerson(String name) {
		for(Person p : persons) {
			if(p.getName().equals(name)) {
				return p;
			}
		}
		
		return null;
	}

	public Collection<Person> getPersons() {
		return this.persons;
	}

	public final String getPersonMap() {
		return persons.toString();
	}

	/**
	 * @param person Person from what the ancestors are returned
	 * @return Ancestors from person
	 */
	public Collection<Person> getAncestors(Person person) {
		Collection<Person> result = new ArrayList<>();

		if(this.getPerson(person.getFathersName()) != null) {
			result.add(this.getPerson(person.getFathersName()));
			result.addAll(this.getAncestors(this.getPerson(person.getFathersName())));
		}
		if(this.getPerson(person.getMothersName()) != null) {
			result.add(this.getPerson(person.getMothersName()));
			result.addAll(this.getAncestors(this.getPerson(person.getMothersName())));
		}
		
		return result;
	}
	
	/**
	 * @param person Person from what the descendants are returned
	 * @return Descendants from person
	 */
	public Collection<Person> getDescendants(Person person) {
		Collection<Person> result = new ArrayList<>();

		for(Person p : this.getChildren(person)) {
			result.add(p);
			result.addAll(this.getDescendants(p));
		}
		
		return result;
	}
	
	
	/**
	 * @return Map<Person, Integer> Integer is the Number of Ancestors
	 */
	public Map<Person, Integer> getNumberOfAncestors() {
		Map<Person, Integer> result = new HashMap<Person, Integer>();
		
		for(Person p : this.persons) {
			result.put(p, this.getAncestors(p).size());
		}
		
		return result;
	}
	
	/**
	 * @param person Person
	 * @return Number of Ancestors from person
	 */
	public int getNumberOfAncestors(Person person) {	
		return this.getAncestors(person).size();
	}
	
	/**
	 * @param person Person
	 * @return Number of Descendants from person
	 */
	public int getNumberOfDescendants(Person person) {	
		return this.getDescendants(person).size();
	}
	
	/**
	 * @return ap<Person, Integer> Integer is the Number of Descendants
	 */
	public Map<Person, Integer> getNumberOfDescendants() {
		Map<Person, Integer> result = new HashMap<Person, Integer>();
		
		for(Person p : this.persons) {
			result.put(p, this.getDescendants(p).size());
		}
		
		return result;
	}
	
	/**
	 * Calculates the number of common ancestors of the persons in pers
	 * @param pers Collection of persons
	 * @return Number of same ancestors
	 */
	public Collection<Person> getCommonAncestors(Collection<Person> pers) {
		Collection<Person> result = new ArrayList();
		boolean flag = true;
		
		for(Person p : pers) {
			if(flag) {
				result = this.getAncestors(p);
				flag = false;
			}
			else {
				result.retainAll(this.getAncestors(p));
			}
		}
		
		return result;
	}
	
	/**
	 * Calculates the number of common descendants of the persons in pers
	 * @param pers Collection of persons
	 * @return Number of same descendants
	 */
	public Collection<Person> getCommonDescendants(Collection<Person> pers) {
		Collection<Person> result = new ArrayList();
		boolean flag = true;
		
		for(Person p : pers) {
			if(flag) {
				result = this.getDescendants(p);
				flag = false;
			}
			else {
				result.retainAll(this.getDescendants(p));
			}
		}
		
		return result;
	}

	/**
	 * Returns a Comparator<Person>, that sorts after number of ancestors (DESC) and name (ASC)
	 * @return The Comparator
	 */
	public Comparator<Person> getAncestorComparator() {
		Comparator<Person> comp = new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				if(getAncestors(o1).size() > getAncestors(o2).size()) {
					return -1;
				}
				else if (getAncestors(o1).size() < getAncestors(o2).size()) {
					return 1;
				}
				else {
					return (o1.getName().compareTo(o2.getName()));
				}
			}
		};
		return comp;
	}

	/**
	 * Returns a Comparator<Person>, that sorts after number of descendents (DESC) and name (ASC)
	 * @return The Comparator
	 */
	public Comparator<Person> getDescendentsComparator() {
		Comparator<Person> comp = new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				if(getDescendants(o1).size() > getDescendants(o2).size()) {
					return -1;
				}
				else if (getDescendants(o1).size() < getDescendants(o2).size()) {
					return 1;
				}
				else {
					return (o1.getName().compareTo(o2.getName()));
				}
			}
		};
		return comp;
	}
	
	public Collection<Person> getChildren(Person person) {
		Collection<Person> result = new ArrayList<>();
		
		for(Person p : this.persons) {
			if(person.getName().equals(p.getMothersName()) || person.getName().equals(p.getFathersName())) {
				result.add(p);
			}
		}
		
		return result;
	}
}
