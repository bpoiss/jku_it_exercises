package chat;

public abstract class Chat {
	public static final String SERVER_HOSTNAME = "localhost";
    public static final int SERVER_PORT = 2000;
    public static final String START_MESSAGE = "===============================================\r\n" +
		"Press enter for input! Enter 'x' to terminate!\r\n" +
		"===============================================";
}
