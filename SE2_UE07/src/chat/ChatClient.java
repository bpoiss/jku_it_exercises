package chat;

import java.io.*;
import java.net.*;

public class ChatClient extends Chat {
    
	public static void main(String[] args) throws InterruptedException {
		try {
			Socket socket = new Socket(SERVER_HOSTNAME, SERVER_PORT);

			System.out.println("Server IP: " + SERVER_HOSTNAME);
			System.out.println("Port number: " + SERVER_PORT);
			System.out.println("++ Connected to server");
			System.out.println(START_MESSAGE);
			new Chatter(socket);
			socket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
