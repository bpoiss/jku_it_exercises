package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class ChatReader implements Runnable {
	Socket socket;
	BufferedReader in;
	Object sync;


	public ChatReader(Socket socket) throws IOException {
		this.socket = socket;
		this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	@Override
	public void run() {
		String line = "";
		while (line != null) {
			try {
				line = in.readLine();
				synchronized (sync) { 
					System.out.println(line == null ? "---Partner has quit---" : "Partner: " + line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			socket.shutdownInput();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setSync(Object sync) {
		this.sync = sync;
	}
}
