package chat;

import java.io.*;
import java.net.*;

public class ChatServer extends Chat {
	public static void main(String[] args) {
		String line = "";
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		ServerSocket serverSocket;

		try {
			serverSocket = new ServerSocket(SERVER_PORT);
			while(!line.equals("n")) {
				System.out.println("Port number: " + SERVER_PORT);
				System.out.println("Waiting for client request");
				Socket socket = serverSocket.accept();
				System.out.println("++ Connected to client");
				System.out.println(START_MESSAGE);
				
				new Chatter(socket);

				socket.close();
				
				System.out.print("Wait for next client? (y | n): ");
				line = in.readLine();
				
				while(!(line.equals("n") || line.equals("y"))) {
					System.out.println("Invalid option!");
					System.out.print("Wait for next client? (y | n): ");
					line = in.readLine();
				}
			}

			System.out.print("Server has shutdown");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
