package chat;

import java.io.*;
import java.net.Socket;

public class ChatWriter implements Runnable {
	Socket socket;
	PrintWriter out;
	BufferedReader in;
	Object sync;

	public ChatWriter(Socket socket) throws IOException {
		this.socket = socket;
		this.out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		this.in = new BufferedReader(new InputStreamReader(System.in));
	}

	@Override
	public void run() {
		String line = "";
		while (!line.equals("x")) {
			try {
				line = in.readLine();
				if (line.equals("")) {
					synchronized (sync) { 
						System.out.print("------------------------------\r\n"
								+ "Your message: ");

						line = in.readLine();

						System.out.println("------------------------------");
					}
					if (line != null) {
						out.printf(line);
						out.append((char) Character.LINE_SEPARATOR);
						out.flush();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println();
		}
		System.out.println("Good Bye");
		try {
			socket.shutdownOutput();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setSync(Object sync) {
		this.sync = sync;
	}

}
