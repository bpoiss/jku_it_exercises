package chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Chatter {
    protected final Object sync = new Object();
    
	private Thread chatWriter;
	private Thread chatReader;
	
	public Chatter(Socket socket) throws IOException {
		ChatWriter cW = new ChatWriter(socket);
		ChatReader cR = new ChatReader(socket);
		
		cW.setSync(sync);
		cR.setSync(sync);
		
		this.chatWriter = new Thread(cR);
		this.chatReader = new Thread(cW);
		

		chatWriter.start();
		chatReader.start();
		
		try {
			chatReader.join();
			chatWriter.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
