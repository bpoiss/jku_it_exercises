package magicmarbles.gui;

import javax.swing.JButton;

public class FieldButton extends JButton {
	int row;
	int col;

	public FieldButton(String text, int row, int col) {
		super(text);
		this.row = row;
		this.col = col;
	}

	public FieldButton(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}
}
