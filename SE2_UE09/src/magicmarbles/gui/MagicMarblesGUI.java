package magicmarbles.gui;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import magicmarbles.model.FieldImpl;
import magicmarbles.model.GameState;

public class MagicMarblesGUI {
	private static final int MIN_HEIGHT_WIDTH = 3;
	private JFrame frame;
	private JPanel fieldPanel;
	private FieldImpl field;
	private JLabel score;

	public MagicMarblesGUI() {
		this.fieldPanel = new JPanel();
		this.frame = new JFrame("Magic Marbles");
		this.score = new JLabel("0");
	}

	public void start() {
		if (newGame()) {
			initializeFrame();
			drawField();
		}
	}

	private void initializeFrame() {
		// Frame properties
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(600, 600);
		frame.setLocation(100, 100);

		// Menu bar
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenu = new JMenuItem("Exit");
		JMenuItem newMenu = new JMenuItem("New");
		menuBar.add(fileMenu);
		fileMenu.add(exitMenu);
		fileMenu.add(newMenu);
		frame.setJMenuBar(menuBar);

		// Action listeners
		exitMenu.addActionListener(exitHandler);
		newMenu.addActionListener(newHandler);

		// Score Components
		JPanel scoreContainer = new JPanel(new FlowLayout());
		scoreContainer.add(new JLabel("Score: "));
		scoreContainer.add(score);
		fieldPanel
				.setLayout(new GridLayout(field.getHeight(), field.getWidth()));

		// Add Components to Content Pane
		Container contentPane = frame.getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(fieldPanel, BorderLayout.CENTER);
		contentPane.add(scoreContainer, BorderLayout.SOUTH);

		// Display frame
		frame.setVisible(true);
	}

	private boolean newGame() {
		JPanel inputPanel = new JPanel();
		JTextField widthInput = new JTextField(null, 3);
		JTextField heightInput = new JTextField(null, 3);
		inputPanel.add(new JLabel("Width: "));
		inputPanel.add(widthInput);
		inputPanel.add(new JLabel("Height: "));
		inputPanel.add(heightInput);
		inputPanel.add(new JLabel("Minimum Height/Width: " + MIN_HEIGHT_WIDTH));

		int result = 0, width = 0, height = 0;
		boolean correctInput = false;

		while (!correctInput) {
			result = JOptionPane.showConfirmDialog(null, inputPanel,
					"New Game", JOptionPane.OK_CANCEL_OPTION);

			if (result == JOptionPane.OK_OPTION) {
				try {
					width = Integer.parseInt(widthInput.getText());
					height = Integer.parseInt(heightInput.getText());
					if (width >= 3 && height >= 3) {
						correctInput = true;
						this.field = new FieldImpl(width, height);
						fieldPanel.removeAll();
						fieldPanel.setLayout(new GridLayout(field.getHeight(),
								field.getWidth()));
						drawField();
					}
				} catch (NumberFormatException e) {
				}
			} else {
				return false;
			}
		}
		return true;
	}

	private void drawField() {
		boolean initialized = false;
		FieldButton btn;

		if (fieldPanel.getComponentCount() > 0)
			initialized = true;

		for (int row = 0; row < field.getHeight(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if (!initialized) {
					btn = new FieldButton(row, col);
					btn.addActionListener(doMove);
					fieldPanel.add(btn);
				} else
					btn = (FieldButton) fieldPanel.getComponent(row
							* field.getWidth() + col);

				switch (field.getFieldState(row, col)) {
				case RED:
					btn.setBackground(new Color(255, 0, 0));
					btn.setEnabled(true);
					break;
				case GREEN:
					btn.setBackground(new Color(0, 255, 0));
					btn.setEnabled(true);
					break;
				case BLUE:
					btn.setBackground(new Color(0, 0, 255));
					btn.setEnabled(true);
					break;
				default:
					btn.setBackground(new Color(0, 0, 0));
					btn.setEnabled(false);
					break;
				}
			}
		}
		fieldPanel.revalidate();
		score.setText(String.valueOf(field.getGamePoints()));
	}

	private ActionListener exitHandler = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			frame.dispose();
		}
	};
	private ActionListener newHandler = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			newGame();
		}
	};
	private ActionListener doMove = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			field.select(((FieldButton) e.getSource()).getRow(),
					((FieldButton) e.getSource()).getCol());
			drawField();
			checkGameState();
		}

		private void checkGameState() {
			if (field.getGameState() == GameState.END) {
				int dialogResult = JOptionPane.showConfirmDialog(null,
						new String[] { "Score: " + field.getGamePoints(),
								"Play again?" }, "Game Over.",
						JOptionPane.YES_NO_OPTION);
				if (dialogResult == JOptionPane.YES_OPTION)
					newGame();
				else
					System.exit(0);
			}
		}
	};
}
