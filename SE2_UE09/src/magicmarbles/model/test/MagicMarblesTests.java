package magicmarbles.model.test;

import junit.framework.Assert;

import magicmarbles.model.FieldImpl;
import magicmarbles.model.FieldState;
import magicmarbles.model.GameState;

import org.junit.Test;

public class MagicMarblesTests {

	@Test
	public void testFieldImplConstructor() {
		FieldState[][] field = MagicMarblesTests.getExampleFieldLayout();

		FieldImpl fieldImpl = new FieldImpl(field);
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {

				Assert.assertEquals(field[i][j], fieldImpl.getFieldState(i, j));
			}
		}
	}

	@Test
	public void testFieldImplConstructor2() { // Not possible to start that test
												// case with well-defind
												// starting condition
		int width = 5;
		int height = 7;
		FieldImpl fieldImpl = new FieldImpl(width, height);
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				Assert.assertTrue(fieldImpl.getFieldState(i, j) instanceof FieldState);
				Assert.assertNotSame(fieldImpl.getFieldState(i, j),
						FieldState.EMPTY);
			}
		}
	}
	
	@Test
	public void testFieldGameWon() {
		FieldState[][] field = MagicMarblesTests.getExampleFieldLayout();
		FieldImpl fieldImpl = new FieldImpl(field);

		fieldImpl.select(2 - 1, 5 - 1);
		fieldImpl.select(4 - 1, 4 - 1);
		fieldImpl.select(1 - 1, 3 - 1);
		Assert.assertEquals(154, fieldImpl.getGamePoints());
		Assert.assertEquals(GameState.END, fieldImpl.getGameState());
	}
	
	@Test
	public void testFieldGameLost() {
		FieldState[][] field = MagicMarblesTests.getExampleFieldLayout();
		FieldImpl fieldImpl = new FieldImpl(field);

		fieldImpl.select(1 - 1, 1 - 1);
		fieldImpl.select(3 - 1, 3 - 1);
		fieldImpl.select(4 - 1, 5 - 1);
		Assert.assertEquals(137, fieldImpl.getGamePoints());
		Assert.assertEquals(GameState.END, fieldImpl.getGameState());
	}
	
	@Test
	public void testTurn() {
		FieldState[][] field = MagicMarblesTests.getExampleFieldAfterTurn();
		FieldImpl fieldImpl = new FieldImpl(MagicMarblesTests.getExampleFieldLayout());

		fieldImpl.select(4-1, 5-1);

		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				Assert.assertEquals(field[i][j], fieldImpl.getFieldState(i, j));
			}
		}
	}
	
	@Test
	public void testWrongTurn() {
		FieldState[][] field = MagicMarblesTests.getExampleFieldLayout();
		FieldImpl fieldImpl = new FieldImpl(field);

		fieldImpl.select(10, 10);

		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				Assert.assertEquals(field[i][j], fieldImpl.getFieldState(i, j));
			}
		}
	}

	/**
	 * @return An example filled field
	 */
	private static FieldState[][] getExampleFieldLayout() {
		return new FieldState[][] 
				{
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.BLUE
				},
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.GREEN
				},
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.GREEN
				},
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.GREEN
				},
				};
	}
	
	/**
	 * @return The example field how it should look after select(4-1, 5-1)
	 */
	private static FieldState[][] getExampleFieldAfterTurn() {
		return new FieldState[][] 
				{
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.EMPTY
				},
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.EMPTY
				},
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.EMPTY
				},
				{
					 FieldState.BLUE ,  FieldState.BLUE ,
					 FieldState.RED ,  FieldState.RED ,  FieldState.BLUE
				},
				};
	}

}
