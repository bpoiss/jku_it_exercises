package magicmarbles.controller;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import magicmarbles.model.FieldModel;
import magicmarbles.view.MarblesGridView;
import magicmarbles.view.MarblesScoreView;

public class GUIController {
	private static final int MIN_HEIGHT_WIDTH = 3;
	private JFrame frame;
	private FieldModel model;
	private MarblesGridView gridView;
	private MarblesScoreView scoreView;

	public GUIController() {
		this.frame = new JFrame("Magic Marbles");
	}
	public boolean newGame() {
		JPanel inputPanel = new JPanel();
		JTextField widthInput = new JTextField(null, 3);
		JTextField heightInput = new JTextField(null, 3);
		inputPanel.add(new JLabel("Width: "));
		inputPanel.add(widthInput);
		inputPanel.add(new JLabel("Height: "));
		inputPanel.add(heightInput);
		inputPanel.add(new JLabel("Minimum Height/Width: " + MIN_HEIGHT_WIDTH));

		int result = 0, width = 0, height = 0;
		boolean correctInput = false;

		while (!correctInput) {
			result = JOptionPane.showConfirmDialog(null, inputPanel,
					"New Game", JOptionPane.OK_CANCEL_OPTION);

			if (result == JOptionPane.OK_OPTION) {
				try {
					width = Integer.parseInt(widthInput.getText());
					height = Integer.parseInt(heightInput.getText());
					if (width >= 3 && height >= 3) {
						correctInput = true;
						this.model = new FieldModel(width, height);
						this.gridView = new MarblesGridView(this.model);
						initializeFrame();
					}
				} catch (NumberFormatException e) {
				}
			} else {
				return false;
			}
		}
		return true;
	}
	private void initializeFrame() {
		// Frame properties
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(600, 600);
		frame.setLocation(100, 100);

		// Menu bar
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem exitMenu = new JMenuItem("Exit");
		JMenuItem newMenu = new JMenuItem("New");
		menuBar.add(fileMenu);
		fileMenu.add(exitMenu);
		fileMenu.add(newMenu);
		frame.setJMenuBar(menuBar);

		// Action listeners
		exitMenu.addActionListener(exitHandler);
		newMenu.addActionListener(newHandler);

		// Score Components
		scoreView = new MarblesScoreView(this.model);		

		// Add Components to Content Pane
		Container contentPane = frame.getContentPane();
		contentPane.removeAll();
		contentPane.setLayout(new BorderLayout());

		contentPane.add(gridView, BorderLayout.CENTER);
		contentPane.add(scoreView, BorderLayout.SOUTH);

		// Display frame
		frame.setVisible(true);
	}
	private ActionListener exitHandler = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			frame.dispose();
		}
	};
	private ActionListener newHandler = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			newGame();
		}
	};
}
