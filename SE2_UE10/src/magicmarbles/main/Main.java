package magicmarbles.main;

import magicmarbles.controller.GUIController;

public class Main {
	
	public static void main(String[] args) {
		// Set useLegacyMergeSort because of bug in jdk7 according to:
		// http://stackoverflow.com/questions/13575224/comparison-method-violates-its-general-contract-timsort-and-gridlayout
		// Needed for fieldsize greater than 90*90
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");

		GUIController controller = new GUIController();
		controller.newGame();
	}

}
