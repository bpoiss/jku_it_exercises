package magicmarbles.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class FieldModel implements Field {
	private int width, height, points = 0;
	private FieldState[][] field;
	private GameState gameState = GameState.RUNNING;
	private List<MagicMarblesListener> listeners = new ArrayList<>();

	/*
	 * constructor which allows deterministic initialization of field -> useful
	 * for testing
	 */
	public FieldModel(FieldState initialMarbles[][]) {
		this.height = initialMarbles.length;
		this.width = initialMarbles[0].length;
		this.field = initialMarbles;
	}
	public FieldModel(int width, int height) {
		assert !((width < 1 && height < 1) || (width == 1 && height == 1)) : "Invalid field size!";
		
		this.field = new FieldState[height][width];
		this.width = width;
		this.height = height;

		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				this.field[row][col] = this.getRandomFieldState();
			}
		}
		
		this.updateGameState();
	}
	@Override
	public int getWidth() {
		return this.width;
	}
	@Override
	public int getHeight() {
		return this.height;
	}
	@Override
	public GameState getGameState() {
		return this.gameState;
	}
	@Override
	public int getGamePoints() {
		return this.points;
	}
	@Override
	public FieldState getFieldState(int row, int col) {
		try {
			return this.field[row][col];
		} catch (IndexOutOfBoundsException e) {
			return FieldState.EMPTY;
		}
	}
	@Override
	public void select(int row, int col) {
		if(this.getFieldState(row, col) != FieldState.EMPTY) {
			boolean[][] itemsToRemove = this.getItemsToRemove(row, col);
			int itemCounter = 0;
			
			for(int i = 0; i < itemsToRemove.length; i++) {
				for(int j = 0; j < itemsToRemove[i].length; j++) {
					if(itemsToRemove[i][j])
						itemCounter++;
				}
			}
			
			if (itemCounter > 1) {
				for(int currentRow = 0; currentRow < itemsToRemove.length; currentRow++) {
					for(int currentCol = 0; currentCol < itemsToRemove[currentRow].length; currentCol++) {
						if(itemsToRemove[currentRow][currentCol])
							field[currentRow][currentCol] = FieldState.EMPTY;
					}
				}
	
				this.calculatePoints(itemCounter);
				this.reAdjust();
				this.updateGameState();
			}
			this.fireChangeEvent();
		}
	}
	public FieldState[][] getField() {
		return this.field;
	}
	
	public void addMagicMarblesListener(MagicMarblesListener listener) {
		listeners.add(listener);
	}
	public void removeMagicMarblesListener(MagicMarblesListener listener) {
		listeners.remove(listener);
	}
	protected void fireChangeEvent() {
		MagicMarblesEvent evt = new MagicMarblesEvent(this, this.getField());
		for (MagicMarblesListener l : listeners) {
			l.fieldChanged(evt);
		}
	}
	
	private boolean[][] getItemsToRemove(int startItemRow, int startItemCol) {
		return getItemsToRemove(startItemRow, startItemCol,
				new boolean[this.getHeight()][this.getWidth()],
				this.getFieldState(startItemRow, startItemCol));
	}
	private boolean[][] getItemsToRemove(int row, int col,
			boolean[][] itemsToRemove, FieldState fieldState) {
		if (row < 0 || col < 0 || row >= this.getHeight()
				|| col >= this.getWidth()
				|| fieldState != this.getFieldState(row, col)) {
			return itemsToRemove;
		}

		if (itemsToRemove[row][col] != false) {
			return itemsToRemove;
		} else {
			itemsToRemove[row][col] = true;
		}

		itemsToRemove = this.getItemsToRemove(row - 1, col, itemsToRemove,
				fieldState);
		itemsToRemove = this.getItemsToRemove(row + 1, col, itemsToRemove,
				fieldState);
		itemsToRemove = this.getItemsToRemove(row, col - 1, itemsToRemove,
				fieldState);
		itemsToRemove = this.getItemsToRemove(row, col + 1, itemsToRemove,
				fieldState);

		return itemsToRemove;
	}
	private FieldState getRandomFieldState() {
		int pick = new Random().nextInt(FieldState.values().length - 1);
		return FieldState.values()[pick];
	}
	private void reAdjust() {
		for (int row = this.getHeight() - 1; row >= 0; row--) {
			for (int col = this.getWidth() - 1; col >= 0; col--) {
				if (this.getFieldState(row, col) == FieldState.EMPTY) {
					reAdjustColumn(row, col);
				}
			}
		}

		for (int col = 0; col < this.getWidth(); col++) {
			if (field[this.getHeight() - 1][col] == FieldState.EMPTY) {
				reAdjustRows(col);
			}
		}
	}
	private void reAdjustColumn(int row, int col) {
		int counter;
		counter = 0;
		while (this.getFieldState(row, col) == FieldState.EMPTY && row > 0
				&& counter < this.getHeight()) {
			for (int tmpRow = row; tmpRow > 0; tmpRow--) {
				field[tmpRow][col] = field[tmpRow - 1][col];
				field[tmpRow - 1][col] = FieldState.EMPTY;
			}
			counter++;
		}
		field[0][col] = FieldState.EMPTY;
	}
	private void reAdjustRows(int emptyCol) {
		for (int col = emptyCol; col > 0; col--) {
			for (int row = this.getHeight() - 1; row >= 0; row--) {
				field[row][col] = field[row][col - 1];
				field[row][col - 1] = FieldState.EMPTY;
			}
		}
		for (int row = 0; row < this.getHeight(); row++) {
			field[row][0] = FieldState.EMPTY;
		}
	}
	private void calculatePoints(int numberOfRemovedItems) {
		this.points += numberOfRemovedItems * numberOfRemovedItems;
	}
	private void updateGameState() {
		this.gameState = GameState.END;
		for (int row = 0; row < this.getHeight(); row++) {
			for (int col = 0; col < this.getWidth(); col++) {
				if (this.hasMatchingNeighbour(row, col)) {
					this.gameState = GameState.RUNNING;
				}
			}
		}
	}
	private boolean hasMatchingNeighbour(int row, int col) {
		FieldState fieldState = this.getFieldState(row, col);

		return fieldState != FieldState.EMPTY
				&& (this.getFieldState(row + 1, col) == fieldState
						|| this.getFieldState(row - 1, col) == fieldState
						|| this.getFieldState(row, col + 1) == fieldState || this
						.getFieldState(row, col - 1) == fieldState);
	}
}
