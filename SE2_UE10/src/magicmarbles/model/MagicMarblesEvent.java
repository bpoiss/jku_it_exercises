package magicmarbles.model;

import java.util.EventObject;


@SuppressWarnings("serial")
public class MagicMarblesEvent extends EventObject {
	
	private final FieldState[][] field;

	public MagicMarblesEvent(FieldModel source, FieldState[][] field) {
		super(source);
		this.field = field;
	}

	public FieldState[][] getField() {
		return this.field;
	}
}
