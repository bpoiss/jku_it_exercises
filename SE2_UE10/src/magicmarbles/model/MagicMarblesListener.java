package magicmarbles.model;

import java.util.EventListener;

public interface MagicMarblesListener extends EventListener {
	
	public void fieldChanged(MagicMarblesEvent evt);
	
}
