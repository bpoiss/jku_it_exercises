package magicmarbles.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import magicmarbles.model.FieldModel;
import magicmarbles.model.GameState;
import magicmarbles.model.MagicMarblesEvent;
import magicmarbles.model.MagicMarblesListener;

@SuppressWarnings("serial")
public class MarblesGridView extends JPanel {
	private FieldModel model;
	private int width, height;

	public MarblesGridView(FieldModel model) {
		setBackground(Color.BLACK);
		this.model = model;
		this.model.addMagicMarblesListener(magicMarblesListener);
		this.addMouseListener(mouseHandler);
		this.setLayout(new GridLayout(this.model.getHeight(), this.model
				.getWidth()));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int x = 0, y = 0;
		this.height = getHeight() / model.getHeight();
		this.width = getWidth() / model.getWidth();
		
		for (int row = 0; row < model.getHeight(); row++) {
			x = 0;
			for (int col = 0; col < model.getWidth(); col++) {
				switch (model.getFieldState(row, col)) {
				case RED:
					g.setColor(new Color(255, 0, 0));
					g.fillOval(x, y, width, height);
					break;
				case GREEN:
					g.setColor(new Color(0, 255, 0));
					g.fillOval(x, y, width, height);
					break;
				case BLUE:
					g.setColor(new Color(0, 0, 255));
					g.fillOval(x, y, width, height);
					break;
				default:
					break;
				}
				x += width;
			}
			y += height;
		}
	}

	private MouseAdapter mouseHandler = new MouseAdapter() {
		@Override
		public void mouseReleased(java.awt.event.MouseEvent e) {
			model.select((int)Math.floor(e.getY() / height),
					((int)Math.floor(e.getX() / width)));
		}

	};
	private MagicMarblesListener magicMarblesListener = new MagicMarblesListener() {
		@Override
		public void fieldChanged(MagicMarblesEvent e) {
			repaint();
			checkGameState();
		}
	};
	private void checkGameState() {
		if (this.model.getGameState() == GameState.END) {
			JOptionPane.showConfirmDialog(null, "Score: "
					+ this.model.getGamePoints(), "Game Over.",
					JOptionPane.CLOSED_OPTION);
			this.model.removeMagicMarblesListener(magicMarblesListener);
		}
	}
}
