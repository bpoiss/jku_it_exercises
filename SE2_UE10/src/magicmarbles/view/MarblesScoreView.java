package magicmarbles.view;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import magicmarbles.model.FieldModel;
import magicmarbles.model.MagicMarblesEvent;
import magicmarbles.model.MagicMarblesListener;

@SuppressWarnings("serial")
public class MarblesScoreView extends JPanel {
	private FieldModel field;
	private JLabel score = new JLabel("0");
	
	public MarblesScoreView(FieldModel model) {
		this.field = model;
		this.field.addMagicMarblesListener(magicMarblesListener);

		JPanel scoreContainer = new JPanel(new FlowLayout());
		scoreContainer.add(new JLabel("Score: "));
		scoreContainer.add(this.score);	
		this.add(scoreContainer);
	}

	private MagicMarblesListener magicMarblesListener = new MagicMarblesListener() {
		@Override
		public void fieldChanged(MagicMarblesEvent e) {
			score.setText(String.valueOf(field.getGamePoints()));
		}
	};
}
